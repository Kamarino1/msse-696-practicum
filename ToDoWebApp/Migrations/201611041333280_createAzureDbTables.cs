namespace ToDoWebApp.Migrations
{
    using System.Data.Entity.Migrations;
    
    /// <summary>
    /// suppress warning
    /// </summary>
    public partial class createAzureDbTables : DbMigration
    {
        /// <summary>
        /// suppress warning
        /// </summary>
        public override void Up()
        {
            CreateTable(
                "dbo.ChoreRecords",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClanId = c.Int(nullable: false),
                        Chore = c.String(),
                        Reward = c.String(),
                        ChildId = c.String(maxLength: 128),
                        ChoreComplete = c.DateTime(),
                        ChoreSubmitted = c.DateTime(),
                        ChoreApproved = c.Boolean(nullable: false),
                        RewardComplete = c.DateTime(),
                        RewardSubmitted = c.DateTime(),
                        RewardApproved = c.Boolean(nullable: false),
                        DateSubmitted = c.DateTime(nullable: false),
                        ParentId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clans", t => t.ClanId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.ChildId)
                .ForeignKey("dbo.AspNetUsers", t => t.ParentId)
                .Index(t => t.ClanId)
                .Index(t => t.ChildId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        ClanName = c.String(),
                        Name = c.String(),
                        ClanId = c.Int(),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clans", t => t.ClanId)
                .Index(t => t.ClanId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Clans",
                c => new
                    {
                        ClanId = c.Int(nullable: false, identity: true),
                        ClanName = c.String(maxLength: 450),
                    })
                .PrimaryKey(t => t.ClanId)
                .Index(t => t.ClanName, unique: true);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
        }

        /// <summary>
        /// suppress warning
        /// </summary>
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.ChoreRecords", "ParentId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ChoreRecords", "ChildId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "ClanId", "dbo.Clans");
            DropForeignKey("dbo.ChoreRecords", "ClanId", "dbo.Clans");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.Clans", new[] { "ClanName" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "ClanId" });
            DropIndex("dbo.ChoreRecords", new[] { "ParentId" });
            DropIndex("dbo.ChoreRecords", new[] { "ChildId" });
            DropIndex("dbo.ChoreRecords", new[] { "ClanId" });
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.Clans");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.ChoreRecords");
        }
    }
}
