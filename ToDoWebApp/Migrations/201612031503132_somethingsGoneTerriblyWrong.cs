namespace ToDoWebApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class somethingsGoneTerriblyWrong : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "ClanId", "dbo.Clans");
            DropIndex("dbo.AspNetUsers", new[] { "ClanId" });
            AddColumn("dbo.AspNetUsers", "Clan_ClanId", c => c.Int());
            AddColumn("dbo.Clans", "ApplicationUser_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.AspNetUsers", "Clan_ClanId");
            CreateIndex("dbo.AspNetUsers", "clan_ClanId");
            CreateIndex("dbo.Clans", "ApplicationUser_Id");
            AddForeignKey("dbo.Clans", "ApplicationUser_Id", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.AspNetUsers", "clan_ClanId", "dbo.Clans", "ClanId");
            AddForeignKey("dbo.AspNetUsers", "Clan_ClanId", "dbo.Clans", "ClanId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "Clan_ClanId", "dbo.Clans");
            DropForeignKey("dbo.AspNetUsers", "clan_ClanId", "dbo.Clans");
            DropForeignKey("dbo.Clans", "ApplicationUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Clans", new[] { "ApplicationUser_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "clan_ClanId" });
            DropIndex("dbo.AspNetUsers", new[] { "Clan_ClanId" });
            DropColumn("dbo.Clans", "ApplicationUser_Id");
            DropColumn("dbo.AspNetUsers", "Clan_ClanId");
            CreateIndex("dbo.AspNetUsers", "ClanId");
            AddForeignKey("dbo.AspNetUsers", "ClanId", "dbo.Clans", "ClanId");
        }
    }
}
