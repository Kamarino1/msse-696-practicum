﻿using System.Security.Claims;
using System.Security.Principal;

namespace ToDoWebApp.Extensions
{
    /// <summary>
    /// This class adds extension methods to the User Identity
    /// </summary>
    public static class IdentityExtensions
    {
        /// <summary>
        /// Get the Name claim
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public static string GetName(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("Name");
            return (claim != null) ? claim.Value : string.Empty;
        }

        /// <summary>
        /// Get the Clan Name claim
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public static string GetClanName(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("ClanName");
            return (claim != null) ? claim.Value : string.Empty;
        }

        /// <summary>
        /// Get the Email claim
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public static string GetEmail(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("Email");
            return (claim != null) ? claim.Value : string.Empty;
        }
    }
}