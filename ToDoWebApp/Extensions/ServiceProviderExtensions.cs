﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;

namespace ToDoWebApp.Extensions
{
    /// <summary>
    /// This class adds an extension method to the service provider
    /// </summary>
    public static class ServiceProviderExtensions
    {
        /// <summary>
        /// Adds the controller type as a service in the IServiceCollection
        /// </summary>
        /// <param name="services">IServiceCollection</param>
        /// <param name="controllerTypes">Type</param>
        /// <returns></returns>
        public static IServiceCollection AddControllersAsServices(this IServiceCollection services,
           IEnumerable<Type> controllerTypes)
        {
            foreach (var type in controllerTypes)
            {
                services.AddTransient(type);
            }

            return services;
        }
    }
}