﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using System.Text.RegularExpressions;
using PagedList;
using ToDoWebApp.Models;
using ToDoWebApp.AppCode.Business;
using System.Threading.Tasks;

namespace ToDoWebApp.Controllers
{
    /// <summary>
    /// Parent Controller
    /// 
    /// User Must be in "Parent" Role
    /// </summary>
    [Authorize(Roles = "Parent")]
    public class ParentController : Controller
    {
        private IParentMgr _pMgr;

        /// <summary>
        /// Constructor that injects IParentMgr
        /// </summary>
        /// <param name="pMgr">IParentMgr</param>
        public ParentController(IParentMgr pMgr)
        {
            _pMgr = pMgr;
        }

        /// <summary>
        /// GET Index
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
        {
            return View(await _pMgr.GetView(User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET Add - Add new chore
        /// </summary>
        /// <returns></returns>
        public ActionResult Add()
        {
            return PartialView("_AddNewChorePartial");
        }

        /// <summary>
        /// POST Add - Add new chore
        /// </summary>
        /// <param name="model">AddChoreViewModel</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Add(AddChoreViewModel model)
        {
            if (ModelState.IsValid)
            {                
                return PartialView("_ChoreTableViewPartial", await _pMgr.AddChore(model, User.Identity.GetUserId()));
            }

            return PartialView("_ChoreTableViewPartial", await _pMgr.GetView(User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET Edit/(int) - Edit an existing chore
        /// </summary>
        /// <param name="id">ID of chore to Edit</param>
        /// <returns></returns>
        public async Task<ActionResult> Edit(int id)
        {
            return PartialView("_EditChorePartial", await _pMgr.getChoreById(id, User.Identity.GetUserId()));
        }

        /// <summary>
        /// POST Edit/(int) - Edit an existing chroe
        /// </summary>
        /// <param name="id">ID of chore to edit</param>
        /// <param name="model">EditChoreViewModel</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Edit(int id, EditChoreViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return PartialView("_ChoreTableViewPartial", await _pMgr.EditChore(model, User.Identity.GetUserId()));
                }
                return PartialView("_ChoreTableViewPartial", await _pMgr.GetView(User.Identity.GetUserId()));
            }
            catch
            {
                return PartialView("_ChoreTableViewPartial", await _pMgr.GetView(User.Identity.GetUserId()));
            }
        }

        /// <summary>
        /// GET Delete/(int) - Delete chore from awaiting completion list
        /// </summary>
        /// <param name="id">ID of chroe to delete</param>
        /// <returns></returns>
        public async Task<ActionResult> Delete(int id)
        {
            return PartialView("_ChoreTableViewPartial", await _pMgr.DeleteChore(id, User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET ApproveChore/(int) - Approve a chore as being completed
        /// </summary>
        /// <param name="id">ID of chore to approve</param>
        /// <returns></returns>
        public async Task<ActionResult> ApproveChore(int id)
        {
            return PartialView("_ChoreTableViewPartial", await _pMgr.ApproveSubmittedChore(id, User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET DenyChore/(int) - Deny a chore as being completed
        /// </summary>
        /// <param name="id">ID of chore to deny</param>
        /// <returns></returns>
        public async Task<ActionResult> DenyChore(int id)
        {
            return PartialView("_ChoreTableViewPartial", await _pMgr.DenySubmittedChore(id, User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET ApproveReward/(int) - Approve a Reward
        /// </summary>
        /// <param name="id">ID of reward to approve</param>
        /// <returns></returns>
        public async Task<PartialViewResult> ApproveReward(int id)
        {
            return PartialView("_ChoreTableViewPartial", await _pMgr.ApproveSubmittedReward(id, User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET DenyReward/(int) - Deny a Reward
        /// </summary>
        /// <param name="id">ID of reward to Deny</param>
        /// <returns></returns>
        public async Task<ActionResult> DenyReward(int id)
        {
            return PartialView("_ChoreTableViewPartial", await _pMgr.DenySubmittedReward(id, User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET History
        /// </summary>
        /// <param name="sortOrder">string containing how to sort the data for display</param>
        /// <param name="currentFilter">string containing any filters currently applied</param>
        /// <param name="searchString">string containing search parameters</param>
        /// <param name="page">current page number</param>
        /// <returns></returns>
        public async Task<ActionResult> History(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder; //set the sort order to maintain sort when navigating pages
            IEnumerable<ParentHistoryViewModel> chores = await _pMgr.GetHistory(User.Identity.GetUserId());

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.Currentfilter = searchString;

            // Search for A Child
            if (!string.IsNullOrWhiteSpace(searchString))
            {
                chores = chores.Where(x => x.ChildName != null && Regex.IsMatch(x.ChildName, searchString, RegexOptions.IgnoreCase));
            }

            // The following region uses ViewBag data and the returned sortOrder variable to run through
            // a switch statement that sorts the data ascending or descending accordingly (nulls are 
            // considered the lowest value and will end up at the end or beginning, respectively)
            #region Sort Functionality (ViewBag and Switch Statement)

            ViewBag.DateEnteredSortParam = String.IsNullOrEmpty(sortOrder) ? "dateEntered_desc" : "";
            ViewBag.ChildSortParam = sortOrder == "Child" ? "child_desc" : "Child";
            ViewBag.ChoreSortParam = sortOrder == "Chore" ? "chore_desc" : "Chore";
            ViewBag.ChoreSubSortParam = sortOrder == "ChoreSub" ? "choreSub_ascend" : "ChoreSub";
            ViewBag.RewardSubmitParam = sortOrder == "RewardSub" ? "rewardSub_ascend" : "RewardSub";
            ViewBag.ChoreApproveSortParam = sortOrder == "ChoreApprove" ? "choreApprove_ascend" : "ChoreApprove";
            ViewBag.RewardSortParam = sortOrder == "Reward" ? "reward_desc" : "Reward";
            ViewBag.RewardApproveSortParam = sortOrder == "RewardApprove" ? "rewardApprove_ascend" : "RewardApprove";
            ViewBag.ParentSortParam = sortOrder == "Parent" ? "parent_desc" : "Parent";

            switch (sortOrder)
            {
                case "dateEntered_desc":
                    chores = chores.OrderByDescending(x => x.DateSubmitted);
                    break;
                case "Child":
                    chores = chores
                        .OrderByDescending(x => x.ChildName);
                    break;
                case "child_desc":
                    chores = chores
                        .OrderBy(x => x.ChildName);
                    break;
                case "Chore":
                    chores = chores.OrderBy(x => x.Chore);
                    break;
                case "chore_desc":
                    chores = chores.OrderByDescending(x => x.Chore);
                    break;
                case "ChoreSub":
                    chores = chores.OrderByDescending(x => x.ChoreSubmitted);
                    break;
                case "choreSub_ascend":
                    chores = chores.OrderBy(x => x.ChoreSubmitted);
                    break;
                case "RewardSub":
                    chores = chores.OrderByDescending(x => x.RewardSubmitted);
                    break;
                case "rewardSub_ascend":
                    chores = chores.OrderBy(x => x.RewardSubmitted);
                    break;
                case "ChoreApprove":
                    chores = chores.OrderByDescending(x => x.ChoreComplete);
                    break;
                case "choreApprove_ascend":
                    chores = chores.OrderBy(x => x.ChoreComplete);
                    break;
                case "Reward":
                    chores = chores.OrderBy(x => x.Reward);
                    break;
                case "reward_desc":
                    chores = chores.OrderByDescending(x => x.Reward);
                    break;
                case "RewardApprove":
                    chores = chores.OrderByDescending(x => x.RewardComplete);
                    break;
                case "rewardApprove_ascend":
                    chores = chores.OrderBy(x => x.RewardComplete);
                    break;
                case "Parent":
                    chores = chores.OrderByDescending(x => x.ParentName);
                    break;
                case "parent_desc":
                    chores = chores.OrderBy(x => x.ParentName);
                    break;
                default:
                    chores = chores.OrderBy(s => s.DateSubmitted);
                    break;
            }

            #endregion

            int pageSize = 15; //number of records to show per page
            int pageNumber = (page ?? 1);
            return View(chores.ToList().ToPagedList(pageNumber, pageSize));
        }
    }
}
