﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using PagedList;
using ToDoWebApp.Models;
using ToDoWebApp.AppCode.Business;
using System.Threading.Tasks;

namespace ToDoWebApp.Controllers
{
    /// <summary>
    /// Child Contoller, must be in "Child" Role
    /// </summary>
    [Authorize(Roles = "Child")]
    public class ChildController : Controller
    {
        private IChildMgr _mgr;

        /// <summary>
        /// Constructor with IChildMgr Injected
        /// </summary>
        /// <param name="cMgr"></param>
        public ChildController(IChildMgr cMgr)
        {
            _mgr = cMgr;
        }

        /// <summary>
        /// GET Index
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
        {
            return View(await _mgr.GetView(User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET SubmitChore/(int)
        /// </summary>
        /// <param name="id">Id of chore to submit</param>
        /// <returns></returns>
        public async Task<ActionResult> SubmitChore(int id)
        {
            return PartialView("_ChoreTableViewPartial", await _mgr.SubmitChore(id, User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET RemoveChore/(int)
        /// </summary>
        /// <param name="id">Id of chore to remove</param>
        /// <returns></returns>
        public async Task<ActionResult> RemoveChore(int id)
        {
            return PartialView("_ChoreTableViewPartial", await _mgr.RemoveChoreFromSubmittal(id, User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET RemoveReward/(int)
        /// </summary>
        /// <param name="id">ID of reward to remove</param>
        /// <returns></returns>
        public async Task<ActionResult> RemoveReward(int id)
        {
            return PartialView("_ChoreTableViewPartial", await _mgr.RemoveRewardFromSubmittal(id, User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET SubmitReward/(int)
        /// </summary>
        /// <param name="id">ID of reward to submit</param>
        /// <returns></returns>
        public async Task<ActionResult> SubmitReward(int id)
        {
            return PartialView("_ChoreTableViewPartial", await _mgr.SubmitReward(id, User.Identity.GetUserId()));
        }

        /// <summary>
        /// GET History
        /// </summary>
        /// <param name="sortOrder">string containing how to sort the data</param>
        /// <param name="page">int containing page to veiw</param>
        /// <returns></returns>
        public async Task<ActionResult> History(string sortOrder, int? page)
        {
            ViewBag.CurrentSort = sortOrder; //set the sort order to maintain sort when navigating pages
            IEnumerable<ChildHistoryViewModel> chores = await _mgr.GetHistory(User.Identity.GetUserId());

            // The following region uses ViewBag data and the returned sortOrder variable to run through
            // a switch statement that sorts the data ascending or descending accordingly (nulls are 
            // considered the lowest value and will end up at the end or beginning, respectively)
            #region Sort Functionality (ViewBag and Switch Statement)

            ViewBag.ChoreSubSortParam = String.IsNullOrEmpty(sortOrder) ? "choreSub_ascend" : "";
            ViewBag.ChoreSortParam = sortOrder == "Chore" ? "chore_desc" : "Chore";
            ViewBag.RewardSubmitParam = sortOrder == "RewardSub" ? "rewardSub_ascend" : "RewardSub";
            ViewBag.ChoreApproveSortParam = sortOrder == "ChoreApprove" ? "choreApprove_ascend" : "ChoreApprove";
            ViewBag.RewardSortParam = sortOrder == "Reward" ? "reward_desc" : "Reward";
            ViewBag.RewardApproveSortParam = sortOrder == "RewardApprove" ? "rewardApprove_ascend" : "RewardApprove";

            switch (sortOrder)
            {
                case "Chore":
                    chores = chores.OrderBy(x => x.Chore);
                    break;
                case "chore_desc":
                    chores = chores.OrderByDescending(x => x.Chore);
                    break;
                case "choreSub_ascend":
                    chores = chores.OrderBy(x => x.ChoreSubmitted);
                    break;
                case "RewardSub":
                    chores = chores.OrderByDescending(x => x.RewardSubmitted);
                    break;
                case "rewardSub_ascend":
                    chores = chores.OrderBy(x => x.RewardSubmitted);
                    break;
                case "ChoreApprove":
                    chores = chores.OrderByDescending(x => x.ChoreComplete);
                    break;
                case "choreApprove_ascend":
                    chores = chores.OrderBy(x => x.ChoreComplete);
                    break;
                case "Reward":
                    chores = chores.OrderBy(x => x.Reward);
                    break;
                case "reward_desc":
                    chores = chores.OrderByDescending(x => x.Reward);
                    break;
                case "RewardApprove":
                    chores = chores.OrderByDescending(x => x.RewardComplete);
                    break;
                case "rewardApprove_ascend":
                    chores = chores.OrderBy(x => x.RewardComplete);
                    break;
                default:
                    chores = chores.OrderByDescending(x => x.ChoreSubmitted);
                    break;
            }

            #endregion

            int pageSize = 15; //number of records per page
            int pageNumber = (page ?? 1);
            return View(chores.ToList().ToPagedList(pageNumber, pageSize));
        }
    }
}
