﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ToDoWebApp.Models;
using ToDoWebApp.AppCode.Business;

namespace ToDoWebApp.Controllers
{
    /// <summary>
    /// The controller for Account pages.
    /// 
    /// Doesn't make sense to use XML documentation for the controllers,
    /// but for consistency sake, here we are. And to supress thos damn
    /// warnings...
    /// </summary>
    [Authorize]
    public class AccountController : Controller
    {
        private IUserMgr _mgr;

        /// <summary>
        /// Constructor that takes an IUserMgr for DI
        /// </summary>
        /// <param name="mgr">IUserMgr</param>
        public AccountController(IUserMgr mgr)
        {
            _mgr = mgr;
        }

        /// <summary>
        /// GET Login
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            if (Request.Cookies["clanName"] != null) //user had previously selected "Remember Clan Name?"
            {
                LoginViewModel view = new LoginViewModel();
                view.ClanName = Request.Cookies["clanName"].Value;
                view.RememberMe = true;
                return View(view);
            }

            return View();
        }

        /// <summary>
        /// POST Login
        /// </summary>
        /// <param name="model">LoginViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.RememberMe) //create cookie to store Clan Name in
                {
                    Response.Cookies["clanName"].Value = model.ClanName;
                    Response.Cookies["clanName"].Expires = DateTime.Now.AddDays(90);
                }
                SignInStatus result = await _mgr.SignIn(model.ClanName, model.UserName, model.Password);
                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToAction("Index", "Home");
                    default:
                        ModelState.AddModelError("", "Invalid login attempt.");
                        break;
                }
            }
            return View(model);
        }

        /// <summary>
        /// POST LogOff
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            HttpContext.GetOwinContext().Authentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// GET ForgotPassword
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        /// <summary>
        /// POST ForgotPassword
        /// </summary>
        /// <param name="model">ForgotPasswordViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                //set the URL to POST Back to and send user a forgot password email
                var callbackUrl = Url.Action("ResetPassword", "Account", new { }, protocol: Request.Url.Scheme);
                await _mgr.SendForgotPasswordCode(model.Email, callbackUrl);

                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }

            // Something failed, redisplay form
            return View(model);
        }

        /// <summary>
        /// GET ForgotPasswordConfirmation
        /// 
        /// Displays a confirmation page after submitting forgot password form, this
        /// should happen regardless of success of previously mentioned activities
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        /// <summary>
        /// GET ResetPassword
        /// </summary>
        /// <param name="UserId">the user ID of the user requesting to reset password</param>
        /// <param name="code">the reset password Token associated with the User ID</param>
        /// <returns></returns>
        [AllowAnonymous]
        public async Task<ActionResult> ResetPassword(string UserId, string code)
        {
            ResetPasswordViewModel model = await _mgr.GetPasswordView(UserId);
            model.Code = code;

            //if there is no code or user Id, show the error page
            return code == null ? View("Error") : UserId == null ? View("Error") : View(model);
        }

        /// <summary>
        /// POST ResetPassword
        /// </summary>
        /// <param name="model">ResetPasswordViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityResult results = await _mgr.ResetPassword(model.Email, model.Code, model.Password);
                if (results.Succeeded)
                {
                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }
                // Don't let the user know why the reset failed, just let them know there was a problem
                // If it was a valid request, they can do it again
                AddErrors(new IdentityResult("An error occured while trying to reset your password!"));

                return View(model);
            }

            return View(model);
        }

        /// <summary>
        /// GET ResetPasswordConfirmation
        /// </summary>
        /// <returns></returns>
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        /// <summary>
        /// GET Register
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated) //user is already logged in, why you tryin to create another account fool?
            {
                return RedirectToAction("Index", "Home");
            }

            RegisterViewModel model = new RegisterViewModel();

            // Seed the drop down list for number of children. If the drop down list needs to be changed
            // only the SeedDDL() method needs to be changed instead of multiple locations (ie here and 
            // in the post method if posting the form failed)
            ViewBag.NumChildrenList = SeedDDL();

            return View(model);
        }

        /// <summary>
        /// POST Register
        /// </summary>
        /// <param name="model">RegisterViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityResult results = await _mgr.Register(model);

                if (results.Succeeded)
                {
                    if (model.RememberMe) //create a cookie to store Clan Name in
                    {
                        Response.Cookies["clanName"].Value = model.ClanName;
                        Response.Cookies["clanName"].Expires = DateTime.Now.AddDays(90);
                    }

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    if (!results.Errors.Contains("FirstFailed"))
                        return RedirectToAction("Failed", "Account");
                }

                AddErrors(results);
            }

            // Seed the drop down list for number of children. If the drop down list needs to be changed
            // only the SeedDDL() method needs to be changed instead of multiple locations (ie here and 
            // in the get method)
            ViewBag.NumChildrenList = SeedDDL();

            return View(model);
        }

        /// <summary>
        /// GET Failed
        /// 
        /// A problem occured during registration
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Failed()
        {
            return View("RegistrationProblems");
        }

        #region Helpers
        /// <summary>
        /// POST doesClanNameExist
        /// </summary>
        /// <param name="ClanName">string of Clan Name</param>
        /// <returns>JSON result containing, FALSE if Clan does exist</returns>
        [HttpPost]
        [AllowAnonymous]
        public JsonResult doesClanNameExist(string ClanName)
        {
            return Json(!(_mgr.ClanExists(ClanName)), JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// POST doesEmailExist
        /// </summary>
        /// <param name="Email">string containing Email to verify</param>
        /// <returns>JSON result containing, FALSE if email does exist</returns>
        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> doesEmailExist(string Email)
        {
            return Json(!(await _mgr.EmailExists(Email)), JsonRequestBehavior.AllowGet);
        }

        // Add errors to the model
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        // Redirect to local URL
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        // Seed the Drop Down List for the Register Views
        private SelectList SeedDDL()
        {
            return new SelectList(new[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }
                .Select(x => new { value = x, text = x }), "value", "text");
        }
        #endregion
    }
}