﻿using System.Web.Mvc;
using ToDoWebApp.Models;

namespace ToDoWebApp.Controllers
{
    /// <summary>
    /// Home Controller
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// GET Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //Redirect to appropriate Home page if user is logged in
            if (User.IsInRole("Parent"))
            {
                return RedirectToAction("Index", "Parent");
            }
            if (User.IsInRole("Child"))
            {
                return RedirectToAction("Index", "Child");
            }

            ViewBag.Message = "Come on in and see what's on your ToDo list!";

            if (Request.Cookies["clanName"] != null) 
            {
                LoginViewModel view = new LoginViewModel();
                view.ClanName = Request.Cookies["clanName"].Value;
                view.RememberMe = true;
                return View(view);
            }
            return View();
        }

        /// <summary>
        /// GET About
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            return View();
        }

        /// <summary>
        /// GET Contact
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            return View();
        }

        /// <summary>
        /// GET Help
        /// </summary>
        /// <returns></returns>
        public ActionResult Help()
        {
            return View();
        }
    }
}