﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using ToDoWebApp.Models;
using ToDoWebApp.Extensions;
using ToDoWebApp.AppCode.Business;

namespace ToDoWebApp.Controllers
{
    /// <summary>
    /// Manage Controller, User must be logged in
    /// </summary>
    [Authorize]
    public class ManageController : Controller
    {
        private IUserMgr _mgr;

        /// <summary>
        /// Constructor that injects IUserMgr
        /// </summary>
        /// <param name="mgr">IUserMgr</param>
        public ManageController(IUserMgr mgr)
        {
            _mgr = mgr;
        }

        /// <summary>
        /// GET Index
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            //Show appropriate view based on User's Role
            if (User.IsInRole("Parent"))
            {
                return View("ParentView", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
            }
            if (User.IsInRole("Child"))
            {
                return View("ChildView", GetChildViewModel());
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// GET AddChild
        /// 
        /// User must be in "Parent" Role
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize (Roles = "Parent")]
        public ActionResult AddChild()
        {
            return PartialView("_AddChildViewPartial");
        }

        /// <summary>
        /// POST AddChild - Add new Child User to Clan
        /// 
        /// User Must be in "Parent" Role
        /// </summary>
        /// <param name="model">AddChildViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Parent")]
        public async Task<ActionResult> AddChild(AddChildViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _mgr.RegisterChild(model, User.Identity.GetClanName());
                if (result.Succeeded)
                {
                    ViewBag.StatusMessageSuccsess = "Successfully Added New Child User!";
                    return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
                }
                else
                {
                    AddErrors(result);
                    ViewBag.StatusMessageFailure = "Errors Ocurred When Adding a New Child User!";
                    return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
                }
            }
            
            // Someting bad happened, lets just ignore it and redisplay the Index
            return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
        }

        /// <summary>
        /// GET AddParent
        /// 
        /// User must be in "Parent" Role
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Parent")]
        public ActionResult AddParent()
        {
            return PartialView("_AddParentViewPartial");
        }

        /// <summary>
        /// POST AddParent - Add new Parent User to Clan
        /// 
        /// User Must be in "Parent" Role
        /// </summary>
        /// <param name="model">AddParentViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Parent")]
        public async Task<ActionResult> AddParent(AddParentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _mgr.RegisterParent(model, User.Identity.GetClanName());
                if (result.Succeeded)
                {
                    ViewBag.StatusMessageSuccsess = "Successfully Added New Parent User!";
                    return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
                }
                else
                {
                    AddErrors(result);
                    ViewBag.StatusMessageFailure = "Errors Ocurred When Adding a New Parent User!";
                    return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
                }
            }

            // Someting bad happened, lets just ignore it and redisplay the Index
            return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
        }

        /// <summary>
        /// GET ChangePassword
        /// 
        /// User must be in "Parent" Role
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Parent")]
        public ActionResult ChangePassword()
        {
            return PartialView("_ChangeUserPasswordPartial");
        }

        /// <summary>
        /// POST ChangePassword - Change User's Password
        /// 
        /// User Must be in "Parent" Role
        /// </summary>
        /// <param name="model">ChangeUserPasswordViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Parent")]
        public async Task<ActionResult> ChangePassword(ChangeUserPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _mgr.ChangePassword(model.OldPassword, model.NewPassword, User.Identity.GetUserId());
                if (result.Succeeded)
                {
                    ViewBag.StatusMessageSuccsess = "Successfully Changed your password!";
                    return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
                }
                else
                {
                    AddErrors(result);
                    ViewBag.StatusMessageFailure = "Errors Ocurred While Changing Your Password!";
                    return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
                }
            }

            return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
        }
        
        /// <summary>
        /// GET UpdateEmail
        /// 
        /// User must be in "Parent" Role
        /// </summary>
        /// <param name="name">Strin</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Parent")]
        public ActionResult UpdateEmail(string name)
        {
            UpdateEmailViewModel view = new UpdateEmailViewModel();
            view.Name = name;

            return PartialView("_UpdateEmailPartial", view);
        }

        /// <summary>
        /// POST UpdateEmail - Change a User's Email
        /// 
        /// User Must be in "Parent" Role
        /// </summary>
        /// <param name="model">UpdateEmailViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Parent")]
        public async Task<ActionResult> UpdateEmail(UpdateEmailViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _mgr.UpdateEmail(model.Email, model.Name, User.Identity.GetClanName());
                if (result.Succeeded)
                {
                    ViewBag.StatusMessageSuccsess = "Successfully Updated " + model.Name + "'s Email Address!";
                    return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
                }
                else
                {
                    AddErrors(result);
                    ViewBag.StatusMessageFailure = "Errors Ocurred While Changing " + model.Name + "'s Email Address!";
                    return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
                }
            }

            return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
        }

        /// <summary>
        /// GET ChangeChildPassword
        /// 
        /// User must be in "Parent" Role
        /// </summary>
        /// <param name="name">string containing name of child whose password will be updated</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Parent")]
        public ActionResult ChangeChildPassword(string name)
        {
            ChangeChildPasswordViewModel model = new ChangeChildPasswordViewModel();
            model.Name = name;

            return PartialView("_ChangeChildPasswordPartial", model);
        }

        /// <summary>
        /// POST ChangeChildPassword - Change a child's password
        /// 
        /// User Must be in "Parent" Role
        /// </summary>
        /// <param name="model">ChangeChildPasswordViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Parent")]
        public async Task<ActionResult> ChangeChildPassword(ChangeChildPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _mgr.ChangeChildPassword(model.Password, User.Identity.GetClanName(), model.Name);
                if (result.Succeeded)
                {
                    ViewBag.StatusMessageSuccsess = "Successfully Changed " + model.Name + "'s Password!";
                    return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
                }
                else
                {
                    AddErrors(result);
                    ViewBag.StatusMessageFailure = "Errors Ocurred While Changing " + model.Name + "'s Password!";
                    return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
                }
            }

            return PartialView("_ParentTableDataPartial", _mgr.GetFamilyUsers(User.Identity.GetUserName(), User.Identity.GetClanName()));
        }

        /// <summary>
        /// GET ChildChangePassword
        /// 
        /// User must be in "Child" Role
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Child")]
        public ActionResult ChildChangePassword()
        {
            return PartialView("_ChildChangePasswordPartial");
        }

        /// <summary>
        /// POST ChildChangePassword - Change a child's password
        /// 
        /// User Must be in "Child" Role
        /// </summary>
        /// <param name="model">ChildChangePasswordViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Child")]
        public async Task<ActionResult> ChildChangePassword(ChildChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _mgr.ChangePassword(model.OldPassword, model.NewPassword, User.Identity.GetUserId());
                if (result.Succeeded)
                {
                    ViewBag.StatusMessageSuccsess = "Successfully Changed your password!";
                    return PartialView("_ChildTableDataPartial", GetChildViewModel());
                }
                else
                {
                    AddErrors(result);
                    ViewBag.StatusMessageFailure = "Errors Ocurred While Changing Your Password!";
                    return PartialView("_ChildTableDataPartial", GetChildViewModel());
                }
            }
            return PartialView("_ChildTableDataPartial", GetChildViewModel());
        }

        /// <summary>
        /// GET ChildUpdateEmail
        /// 
        /// User must be in "Child" Role
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Child")]
        public ActionResult ChildUpdateEmail()
        {
            return PartialView("_ChildUpdateEmailPartial");
        }

        /// <summary>
        /// POST ChildUpdateEmail - Change a child's email
        /// 
        /// User Must be in "Child" Role
        /// </summary>
        /// <param name="model">ChildUpdateEmailViewModel</param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Child")]
        public async Task<ActionResult> ChildUpdateEmail(ChildUpdateEmailViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _mgr.UpdateEmail(model.Email, User.Identity.GetName(), User.Identity.GetClanName());
                if (result.Succeeded)
                {
                    //sign in user to update user identity claims to reflect updated email
                    await _mgr.SignIn(User.Identity.GetUserId());

                    //since current scope of user identity claims has old email, create a new
                    //view and set email to the updated email address
                    ChildManageViewModel viewModel = GetChildViewModel();
                    viewModel.Email = model.Email;

                    ViewBag.StatusMessageSuccsess = "Successfully Updated Your Email Address!";
                    return PartialView("_ChildTableDataPartial", viewModel);
                }
                else
                {
                    AddErrors(result);
                    ViewBag.StatusMessageFailure = "Errors Ocurred While Changing Your Email Address!";
                    return PartialView("_ChildTableDataPartial", GetChildViewModel());
                }
            }
            return PartialView("_ChildTableDataPartial", GetChildViewModel());
        }

        //Add Errors to the Model
        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        // Create the View Model for a Child View
        private ChildManageViewModel GetChildViewModel()
        {
            ChildManageViewModel model = new ChildManageViewModel();
            model.ClanName = User.Identity.GetClanName();
            model.UserName = User.Identity.GetName();
            model.Email = User.Identity.GetEmail();

            return model;
        }
    }
}