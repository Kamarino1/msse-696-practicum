﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using ToDoWebApp.AppCode.Domain;
using System;

namespace ToDoWebApp.AppCode.Service
{
    /// <summary>
    /// Configure the ASP.Net Identity User Manager that this application uses
    /// </summary>
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="store">IUserStore</param>
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        /// <summary>
        /// Create a new ApplicationUserManager Instance
        /// </summary>
        /// <param name="options">User Manager options</param>
        /// <param name="context">IOwinContext</param>
        /// <returns></returns>
        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));

            // Configure validation logic for usernames, use custom user validator because of unique user requirements
            manager.UserValidator = new CustomUserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = true,
                RequireUniqueEmail = false
            };

            // Configure validation logic for passwords
            // Due to the target audience, children and their families,
            // the password requirements are Extremely lax
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 4, //4 is the minimum amount, child users have a requirement of 4 and parents of 6
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Disable lockouts
            manager.UserLockoutEnabledByDefault = false;

            // Attach the Email Service to the User Manager
            manager.EmailService = new EmailService();

            // Define Token Provider and Token rules
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider =
                    new DataProtectorTokenProvider<ApplicationUser>
                       (dataProtectionProvider.Create("ASP.NET Identity"))
                    {
                        TokenLifespan = TimeSpan.FromHours(2)
                    };
            }
            return manager;
        }
    }
}
