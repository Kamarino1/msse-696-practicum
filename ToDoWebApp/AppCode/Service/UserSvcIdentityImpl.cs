﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ToDoWebApp.AppCode.Domain;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;
using System.Data.Entity;

namespace ToDoWebApp.AppCode.Service
{
    /// <summary>
    /// This class implements the IUserSvc interface
    /// 
    /// This class utilizes ASP.NET Identity Framework and OWIN
    /// Security
    /// </summary>
    public class UserSvcIdentityImpl : IUserSvc
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public UserSvcIdentityImpl()
        {
        }

        /// <summary>
        /// Constructor to used to inject the userManager and signInManager
        /// </summary>
        /// <param name="userManager">the Identity User Manager</param>
        /// <param name="signInManager">the Identty Sign In Manager</param>
        public UserSvcIdentityImpl(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        /// <summary>
        /// This method is used to register the first user within the account. Once the user is created,
        /// the clan is created in the database and then the user is updated with the newly created ClanID
        /// foreign key. This should be refactored to use the generic register method and the manager should
        /// handle that logic. This logic is required due to the limititations of the Identity framework
        /// to insert into multiple databases
        /// </summary>
        /// <param name="user">ApplicationUser to register</param>
        /// <param name="password">string containing the user's password</param>
        /// <returns>IdentityResult</returns>
        public async Task<IdentityResult> RegisterFirstUser(ApplicationUser user, string password)
        {
            try
            {
                IdentityResult result = await UserManager.CreateAsync(user, password);

                if (result.Succeeded) //User was successfully created so go ahead and create the Clan
                {
                    Clan clan = new Clan();
                    clan.ClanName = user.ClanName;

                    using (ApplicationDbContext context = new ApplicationDbContext())
                    {
                        context.Clans.Add(clan);
                        context.SaveChanges();
                    }

                    //Update the user with the newly found ClanId to link that user to the clan they
                    user.ClanId = clan.ClanId;
                    user.PrimaryParent = false;

                    await UserManager.UpdateAsync(user);
                }

                return result;
            }
            catch (Exception)
            {
                return new IdentityResult("An Unknown Error ocurred while Registering User!");
            }
        }

        /// <summary>
        /// This method registers a new user in an existing Clan
        /// </summary>
        /// <param name="user">ApplicationUser to register</param>
        /// <param name="password">string containing the password of the User</param>
        /// <returns>Identity Result</returns>
        public async Task<IdentityResult> RegisterUser(ApplicationUser user, string password)
        {
            if (user.ClanId == null || user.ClanId == 0)
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    user.ClanId = context.Clans.First(x => x.ClanName == user.ClanName).ClanId;
                }
            }
            try
            {
                IdentityResult result = await UserManager.CreateAsync(user, password);
                return result;
            }
            catch (Exception)
            {
            }
            return IdentityResult.Failed("An Unknown Error ocurred while Registering User!!");
        }

        /// <summary>
        /// This method adds a user to a role that is found in ApplicationUser.Role
        /// </summary>
        /// <param name="user">ApplicationUser with role</param>
        /// <returns>Identity Result</returns>
        public async Task<IdentityResult> AddToRole(ApplicationUser user)
        {
            try
            {
                return await UserManager.AddToRoleAsync(user.Id, user.Role);
            }
            catch (Exception)
            {
                return new IdentityResult("An Unknown Error ocurred while trying to add Authority to User!");
            }
        }

        /// <summary>
        /// Sign a user into the application
        /// </summary>
        /// <param name="user">ApplicationUser to sign in</param>
        /// <returns>Task</returns>
        public async Task SignIn(ApplicationUser user)
        {
            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        }

        /// <summary>
        /// Sign a usre into the application
        /// </summary>
        /// <param name="name">string containing User Name</param>
        /// <param name="password">sting containing Password</param>
        /// <returns>SignInStatus</returns>
        public async Task<SignInStatus> SignIn(string name, string password)
        {
            try
            {
                return await SignInManager.PasswordSignInAsync(name, password, isPersistent: false, shouldLockout: false);
            }
            catch (Exception)
            {
                return SignInStatus.Failure;
            }
        }

        /// <summary>
        /// Upate user information
        /// </summary>
        /// <param name="user">ApplicationUser with updated information</param>
        /// <returns>IdentityResult</returns>
        public async Task<IdentityResult> UpdateUser(ApplicationUser user)
        {
            try
            {
                return await UserManager.UpdateAsync(user);
            }
            catch (Exception)
            {
                return new IdentityResult("An Unknown Error ocurred while trying to update User's Information!");
            }
        }

        /// <summary>
        /// Updates a user's password
        /// </summary>
        /// <param name="oldPass">The current Password</param>
        /// <param name="newPass">The new Password</param>
        /// <param name="userID">The user's ID (not the user name)</param>
        /// <returns>IdentityResult</returns>
        public async Task<IdentityResult> UpdatePassword(string oldPass, string newPass, string userID)
        {
            try
            {
                return await UserManager.ChangePasswordAsync(userID, oldPass, newPass);
            }
            catch (Exception)
            {
                return new IdentityResult("An Unknown Error ocurred while trying to update Password!");
            }
        }

        /// <summary>
        /// Resets a user's password
        /// </summary>
        /// <param name="token">The reset password token</param>
        /// <param name="newPass">the new passowrd</param>
        /// <param name="userId">the user's ID (not the user name)</param>
        /// <returns>IdentityResult</returns>
        public async Task<IdentityResult> ResetPassword(string token, string newPass, string userId)
        {
            try
            {
                return await UserManager.ResetPasswordAsync(userId, token, newPass);
            }
            catch (Exception)
            {
                return new IdentityResult("An Unknown Error ocurred while trying to update Password!");
            }
        }

        /// <summary>
        /// Get a user by their user Id
        /// </summary>
        /// <param name="id">user Id of user to retrieve</param>
        /// <returns>ApplicationUser</returns>
        public async Task<ApplicationUser> GetUserById(string id)
        {
            try
            {
                return await UserManager.FindByIdAsync(id);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get a user by their email address
        /// </summary>
        /// <param name="email">email address of user to retrieve</param>
        /// <returns>ApplicationUser</returns>
        public async Task<ApplicationUser> GetUserByEmail(string email)
        {
            try
            {
                return await UserManager.FindByEmailAsync(email);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get a user by their user name
        /// </summary>
        /// <param name="userName">user name address of user to retrieve</param>
        /// <returns>ApplicationUser</returns>
        public async Task<ApplicationUser> GetUserByName(string userName)
        {
            try
            {
                return await UserManager.FindByNameAsync(userName);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get all users in a clan
        /// </summary>
        /// <param name="clanName">Clan name of the users to retrieve</param>
        /// <returns>ApplicationUser Collection</returns>
        public ICollection<ApplicationUser> GetUsersByClan(string clanName)
        {
            using (ApplicationDbContext context = new ApplicationDbContext())
            {
                return context.Users.Include(x => x.Roles).Where(x => x.ClanName == clanName).ToList();
            }
        }

        /// <summary>
        /// Geenerates a reset password token
        /// </summary>
        /// <param name="userId">User Id of user to associate token with</param>
        /// <returns>String</returns>
        public async Task<String> getPassToken(string userId)
        {
            return await UserManager.GeneratePasswordResetTokenAsync(userId);
        }

        /// <summary>
        /// Send an email to a user
        /// </summary>
        /// <param name="userId">user Id of user to send email to</param>
        /// <param name="subject">Subject of the email</param>
        /// <param name="body">body of the email</param>
        /// <returns>Task</returns>
        public async Task SendEmail(string userId, string subject, string body)
        {
            await UserManager.SendEmailAsync(userId, subject, body);
        }

        #region Helpers
        private ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
            }
            set
            {
                _signInManager = value;
            }
        }

        private ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }
        #endregion
    }
}