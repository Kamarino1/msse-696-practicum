﻿using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using ToDoWebApp.AppCode.Domain;

namespace ToDoWebApp.AppCode.Service
{
    /// <summary>
    /// This class implements the IChoreSvc interface
    /// 
    /// This Class utilizes Microsoft Entity Framework
    /// to accomplish CRUD operations. 
    /// 
    /// It almost feels like cheating and I should have manually genereated
    /// the SQL statements and communicated to the DB that way
    /// </summary>
    public class ChoreSvcSqlExprsImpl : IChoreSvc
    {
        // Since we are using Homebrew DI for the services layer,
        // we need to handle the context here. However, if we
        // were using ASP.Net Core DI, we could register the DB Context
        // as a service and have Core DI resolve it for us
        private ApplicationDbContext context;

        /// <summary>
        /// Get a chore by its ID
        /// </summary>
        /// <param name="id">The Chore Id to get</param>
        /// <returns>ChoreRecord</returns>
        public ChoreRecord getChoreById(int id)
        {
            using (context = new ApplicationDbContext())
            {
                return context.Chores.Find(id);
            }
        }

        /// <summary>
        /// Add a Chore to the Chore Store
        /// </summary>
        /// <param name="chore">The Chore to add</param>
        public void AddChore(ChoreRecord chore)
        {
            using (context = new ApplicationDbContext())
            {
                context.Chores.Add(chore);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Update an Existing chore
        /// </summary>
        /// <param name="chore">The chore entity containing the updates</param>
        public void UpdateChore(ChoreRecord chore)
        {
            using (context = new ApplicationDbContext())
            {
                var record = context.Chores.Find(chore.Id);
                if (record != null)
                {
                    context.Entry(record).CurrentValues.SetValues(chore);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Remove a chore from the chore store
        /// </summary>
        /// <param name="Id">The ID of the chore to remove</param>
        public void DeleteChore(int Id)
        {
            using (context = new ApplicationDbContext())
            {
                var record = context.Chores.Find(Id);
                if (record != null)
                {
                    context.Chores.Remove(record);
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Get a list of chores associated with the given clan
        /// </summary>
        /// <param name="clanId">The clan ID to obtain a list of chores for</param>
        /// <returns>List of ChoreRecords</returns>
        public List<ChoreRecord> GetChoresByClan(int clanId)
        {
            using (context = new ApplicationDbContext())
            {
                List<ChoreRecord> chores = context.Chores.Include(x => x.Child).Include(x => x.Parent).Where(x => x.ClanId == clanId).ToList();
                return chores;
            }
        }
    }
}