﻿using System;
using System.Web.Configuration;

namespace ToDoWebApp.AppCode.Service
{
    /// <summary>
    /// This Class is an implementation of Homebrew dependency injection
    /// </summary>
    public class Factory
    {
        /// <summary>
        /// Default Contstructor
        /// </summary>
        public Factory() { }

        /// <summary>
        /// Get a service implementation based on its name
        /// </summary>
        /// <param name="svcName">Name of the service that an implementation is desired for</param>
        /// <returns>Service implementation as IService</returns>
        public IService GetService(string svcName)
        {
            Type type;
            Object obj = null;

            try
            {
                type = Type.GetType(GetImplName(svcName));
                obj = Activator.CreateInstance(type);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception occured: {0}", e);
                throw e;
            }

            return (IService)obj;
        }

        //Get the implementation from the WebConfig file
        private string GetImplName(string svcName)
        {
            return WebConfigurationManager.AppSettings[svcName];
        }
    }
}