﻿using System.Collections.Generic;
using ToDoWebApp.AppCode.Domain;

namespace ToDoWebApp.AppCode.Service
{
    /// <summary>
    /// This interface defines the functionality of the Service that handles
    /// CRUD operations for the chores
    /// </summary>
    public interface IChoreSvc : IService
    {
        /// <summary>
        /// Get a chore by its ID
        /// </summary>
        /// <param name="id">The Chore Id to get</param>
        /// <returns>ChoreRecord</returns>
        ChoreRecord getChoreById(int id);

        /// <summary>
        /// Add a Chore to the Chore Store
        /// </summary>
        /// <param name="chore">The Chore to add</param>
        void AddChore(ChoreRecord chore);

        /// <summary>
        /// Update an Existing chore
        /// </summary>
        /// <param name="chore">The chore entity containing the updates</param>
        void UpdateChore(ChoreRecord chore);

        /// <summary>
        /// Remove a chore from the chore store
        /// </summary>
        /// <param name="Id">The ID of the chore to remove</param>
        void DeleteChore(int Id);

        /// <summary>
        /// Get a list of chores associated with the given clan
        /// </summary>
        /// <param name="clanId">The clan ID to obtain a list of chores for</param>
        /// <returns>List of ChoreRecords</returns>
        List<ChoreRecord> GetChoresByClan(int clanId);
    }
}