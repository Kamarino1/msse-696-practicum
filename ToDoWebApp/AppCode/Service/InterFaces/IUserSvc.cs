﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoWebApp.AppCode.Domain;

namespace ToDoWebApp.AppCode.Service
{
    /// <summary>
    /// This interface defines the functionality of the Service that handles
    /// User account functionality
    /// </summary>
    public interface IUserSvc : IService
    {
        /// <summary>
        /// This method is used to register the first user within the account. 
        /// </summary>
        /// <param name="user">ApplicationUser to register</param>
        /// <param name="password">string containing the user's password</param>
        /// <returns>IdentityResult</returns>
        Task<IdentityResult> RegisterFirstUser(ApplicationUser user, string password);

        /// <summary>
        /// This method registers a new user in an existing Clan
        /// </summary>
        /// <param name="user">ApplicationUser to register</param>
        /// <param name="password">string containing the password of the User</param>
        /// <returns>Identity Result</returns>
        Task<IdentityResult> RegisterUser(ApplicationUser user, string password);

        /// <summary>
        /// This method adds a user to a role that is found in ApplicationUser.Role
        /// </summary>
        /// <param name="user">ApplicationUser with role</param>
        /// <returns>Identity Result</returns>
        Task<IdentityResult> AddToRole(ApplicationUser user);

        /// <summary>
        /// Sign a user into the application
        /// </summary>
        /// <param name="user">ApplicationUser to sign in</param>
        /// <returns>Task</returns>
        Task SignIn(ApplicationUser user);

        /// <summary>
        /// Sign a usre into the application
        /// </summary>
        /// <param name="name">string containing User Name</param>
        /// <param name="password">sting containing Password</param>
        /// <returns>SignInStatus</returns>
        Task<SignInStatus> SignIn(string name, string password);

        /// <summary>
        /// Upate user information
        /// </summary>
        /// <param name="user">ApplicationUser with updated information</param>
        /// <returns>IdentityResult</returns>
        Task<IdentityResult> UpdateUser(ApplicationUser user);

        /// <summary>
        /// Updates a user's password
        /// </summary>
        /// <param name="oldPass">The current Password</param>
        /// <param name="newPass">The new Password</param>
        /// <param name="userID">The user's ID (not the user name)</param>
        /// <returns>IdentityResult</returns>
        Task<IdentityResult> UpdatePassword(string oldPass, string newPass, string userID);

        /// <summary>
        /// Resets a user's password
        /// </summary>
        /// <param name="token">The reset password token</param>
        /// <param name="newPass">the new passowrd</param>
        /// <param name="userId">the user's ID (not the user name)</param>
        /// <returns>IdentityResult</returns>
        Task<IdentityResult> ResetPassword(string token, string newPass, string userId);

        /// <summary>
        /// Get a user by their user Id
        /// </summary>
        /// <param name="id">user Id of user to retrieve</param>
        /// <returns>ApplicationUser</returns>
        Task<ApplicationUser> GetUserById(string id);

        /// <summary>
        /// Get a user by their email address
        /// </summary>
        /// <param name="email">email address of user to retrieve</param>
        /// <returns>ApplicationUser</returns>
        Task<ApplicationUser> GetUserByEmail(string email);

        /// <summary>
        /// Get a user by their user name
        /// </summary>
        /// <param name="userName">user name address of user to retrieve</param>
        /// <returns>ApplicationUser</returns>
        Task<ApplicationUser> GetUserByName(string userName);

        /// <summary>
        /// Get all users in a clan
        /// </summary>
        /// <param name="clanName">Clan name of the users to retrieve</param>
        /// <returns>ApplicationUser Collection</returns>
        ICollection<ApplicationUser> GetUsersByClan(string clanName);

        /// <summary>
        /// Geenerates a reset password token
        /// </summary>
        /// <param name="userId">User Id of user to associate token with</param>
        /// <returns>String</returns>
        Task<String> getPassToken(string userId);

        /// <summary>
        /// Send an email to a user
        /// </summary>
        /// <param name="userId">user Id of user to send email to</param>
        /// <param name="subject">Subject of the email</param>
        /// <param name="body">body of the email</param>
        /// <returns>Task</returns>
        Task SendEmail(string userId, string subject, string body);
    }
}