﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ToDoWebApp.AppCode.Domain;

namespace ToDoWebApp.AppCode.Service
{
    /// <summary>
    /// Custom implementationo of UserValidator to handle unique User requirements
    /// </summary>
    /// <typeparam name="TUser">ApplicationUser</typeparam>
    public class CustomUserValidator<TUser> : UserValidator<TUser, string> where TUser : ApplicationUser
    {
        private UserManager<TUser, string> Manager { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="manager"></param>
        public CustomUserValidator(UserManager<TUser, string> manager)
            : base(manager)
        {
            this.Manager = manager;
        }

        /// <summary>
        /// Validate the user through validating the Clan Name,
        /// User Name, and Emal Address
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public override async Task<IdentityResult> ValidateAsync(TUser item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            var errors = new List<string>();
            if (item.PrimaryParent)
            {
                ValidateClanName(item, errors);
            }
            ValidateName(item, errors);
            await ValidateUserName(item, errors);
            if (RequireUniqueEmail && item.Role == "Parent") //Only Parent Role requires Email...how do I encapsulate this?? Make it a user field??
            {
                await ValidateEmail(item, errors);
            }
            if (errors.Count > 0)
            {
                return IdentityResult.Failed(errors.ToArray());
            }
            return IdentityResult.Success;
        }

        //Validate Name
        private void ValidateName(TUser user, List<string> errors)
        {
            if (string.IsNullOrWhiteSpace(user.Name))
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, CustomResources.PropertyTooShort, "Clan Name"));
            }
            else if ((AllowOnlyAlphanumericUserNames && !Regex.IsMatch(user.Name, @"^[A-Za-z0-9@_\/.]+$")) ||
                Regex.IsMatch(user.Name, ApplicationUser.userNameDelimiter))
            {
                // If any characters are not letters or digits, its an illegal user name
                //especially concerning is ApplicationUser.userNameDelimiter as it is inserted into the username
                errors.Add(String.Format(CultureInfo.CurrentCulture, CustomResources.InvalidUserName, user.Name));
            }
        }

        //Validate Clan Name
        private void ValidateClanName(TUser user, List<string> errors)
        {
            if (string.IsNullOrWhiteSpace(user.ClanName))
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, CustomResources.PropertyTooShort, "Clan Name"));
            }
            else if ((AllowOnlyAlphanumericUserNames && !Regex.IsMatch(user.ClanName, @"^[A-Za-z0-9@_\/.]+$")) ||
                Regex.IsMatch(user.Name, ApplicationUser.userNameDelimiter))
            {
                // If any characters are not letters or digits, its an illegal user name
                //especially concerning is ApplicationUser.userNameDelimiter as it is inserted into the username
                errors.Add(String.Format(CultureInfo.CurrentCulture, CustomResources.InvalidClanName, user.ClanName));
            }
            else
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    bool duplicateClanName = context.Clans.Any(x => x.ClanName == user.ClanName);
                    if (duplicateClanName)
                    {
                        errors.Add(String.Format(CultureInfo.CurrentCulture, CustomResources.DuplicateClanName, user.ClanName));
                    }
                }
            }
        }

        //Make sure UserName is not empty and unique
        private async Task ValidateUserName(TUser user, List<string> errors)
        {
            if (string.IsNullOrWhiteSpace(user.UserName) && string.IsNullOrWhiteSpace(user.UserName.Split(ApplicationUser.userNameDelimiter[0]).Last()))
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, CustomResources.PropertyTooShort, "Name"));
            }
            else
            {
                var owner = await Manager.FindByNameAsync(user.UserName);
                if (owner != null && !EqualityComparer<string>.Default.Equals(owner.Id, user.Id))
                {
                    errors.Add(String.Format(CultureInfo.CurrentCulture, CustomResources.DuplicateName, user.Name));
                }
            }
        }

        // make sure email is not empty, valid, and unique
        private async Task ValidateEmail(TUser user, List<string> errors)
        {
            if (!string.IsNullOrWhiteSpace(user.Email))
            {
                if (string.IsNullOrWhiteSpace(user.Email))
                {
                    errors.Add(String.Format(CultureInfo.CurrentCulture, CustomResources.PropertyTooShort, "Email"));
                    return;
                }
                try //verify formatting
                {
                    var m = new MailAddress(user.Email);
                }
                catch (FormatException)
                {
                    errors.Add(String.Format(CultureInfo.CurrentCulture, CustomResources.InvalidEmail, user.Email));
                    return;
                }
            }
            //verify email is unique
            var owner = await Manager.FindByEmailAsync(user.Email);
            if (owner != null && !EqualityComparer<string>.Default.Equals(owner.Id, user.Id))
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, CustomResources.DuplicateEmail, user.Email));
            }
        }
    }
}