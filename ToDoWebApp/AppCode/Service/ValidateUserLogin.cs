﻿using Microsoft.AspNet.Identity;
using System.Linq;
using System.Text.RegularExpressions;
using ToDoWebApp.AppCode.Domain;

namespace ToDoWebApp.AppCode.Service
{
    /// <summary>
    /// This class is used to validate the entered Clan Name and User Name before even attempting to sign the user in
    /// </summary>
    public class ValidateUserLogin
    {
        /// <summary>
        /// Validate the user attempting to log in
        /// 
        /// This isn't really necessary, as there is no logical way I can think of that a user
        /// could hack past the clanName + userNameDelimiter + userName sequence and successfully
        /// log in. A user cannot put the delimiter in their username during registration and if
        /// a user puts it in the log in fields then two would exist in the username they are trying
        /// to sign in with. This class was just an exercise.
        /// </summary>
        /// <param name="clanName">Clan name of user</param>
        /// <param name="name">User name</param>
        /// <returns>IdentityResult</returns>
        public static IdentityResult Validate(string clanName, string name)
        {
            if (clanName == null || name == null)
            {
                return IdentityResult.Failed("Failed");
            }
            bool error = false;

            ValidateClanName(clanName, ref error);
            ValidateName(name, ref error);

            if (error)
            {
                return IdentityResult.Failed("Failed");
            }
            return IdentityResult.Success;
        }

        //Validate Name
        private static void ValidateClanName(string name, ref bool error)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                error = true;
            }
            else if (Regex.IsMatch(name, ApplicationUser.userNameDelimiter))
            {
                // If the user name delimiter is present, then the entered clan name is invalid
                error = true;
            }
            else //validate that the clan exists (we only have to validate the clan name, the username will be validated during the login process)
            {
                using (ApplicationDbContext context = new ApplicationDbContext())
                {
                    bool clanExists = context.Clans.Any(x => x.ClanName == name);
                    if (!clanExists)
                    {
                        error = true;
                    }
                }
            }
        }

        //Validate Clan Name
        private static void ValidateName(string name, ref bool error)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                error = true;
            }
            else if (Regex.IsMatch(name, ApplicationUser.userNameDelimiter))
            {
                // If the user name delimiter is present, then the entered clan name is invalid
                error = true;
            }
        }
    }
}