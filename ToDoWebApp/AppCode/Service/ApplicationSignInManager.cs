﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using ToDoWebApp.AppCode.Domain;

namespace ToDoWebApp.AppCode.Service
{
    /// <summary>
    /// Configure the ASP.Net Identity Sign-in Manager that this application uses
    /// </summary>
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userManager">ApplicationUserManager</param>
        /// <param name="authenticationManager">IAuthenticationManager</param>
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        /// <summary>
        /// Over rides the default CreateUserIdentityAsync method to utilize the 
        /// GenerateUserIdentityAsync method defined in the ApplicationUser Class
        /// </summary>
        /// <param name="user">ApplicationUser object of user to generate an Identity for</param>
        /// <returns>ClaimsIdentity</returns>
        public override async Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return await user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        /// <summary>
        /// Create a new ApplicationSignInManager
        /// </summary>
        /// <param name="options">SignInManager options</param>
        /// <param name="context">IOwinContext</param>
        /// <returns>ApplicationSignInManager</returns>
        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}
