﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ToDoWebApp.AppCode.Domain
{
    /// <summary>
    /// This class defines the Database Contenxt that will be used
    /// with ASP.Net Entity Framework
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        /// <summary>
        /// Create the Database Context
        /// </summary>
        /// <returns>ApplicationDbContext</returns>
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        ///// <summary>
        ///// Configures the default ASP.Net Identity Database, ignoring a number of columns
        ///// </summary>
        ///// <param name="modelBuilder">DbModelBuilder/param>
        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    base.OnModelCreating(modelBuilder);

        //    //Remove unnecessary columns from Identity Users Table
        //    modelBuilder.Entity<IdentityUser>()
        //        .Ignore(x => x.EmailConfirmed)
        //        .Ignore(x => x.PhoneNumber)
        //        .Ignore(x => x.PhoneNumberConfirmed)
        //        .Ignore(x => x.AccessFailedCount)
        //        .Ignore(x => x.LockoutEnabled)
        //        .Ignore(x => x.LockoutEndDateUtc)
        //        .Ignore(x => x.TwoFactorEnabled);
        //}

        /// <summary>
        /// DbSet of Clans registered to application
        /// </summary>
        public DbSet<Clan> Clans { get; set; }

        /// <summary>
        /// DbSet of Chores stored in the Chore Store
        /// </summary>
        public DbSet<ChoreRecord> Chores { get; set; }

    }
}