﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoWebApp.AppCode.Domain
{
    /// <summary>
    /// The purpose of this class is to create a database with single entries to enforce
    /// the unique Clan Name requirement on the database side.
    /// </summary>
    public class Clan
    {
        /// <summary>
        /// Primary Key, Clan Id
        /// </summary>
        [Key]
        public int ClanId { get; set; }

        /// <summary>
        /// Name of the clan
        /// Unique value, max of 450 Characters
        /// </summary>
        [Index(IsUnique = true)]
        [StringLength(450)]
        public string ClanName { get; set; }

        /// <summary>
        /// List of Users belonging to this clan
        /// </summary>
        public virtual ICollection<ApplicationUser> Users { get; set; }

        /// <summary>
        /// List of chores belonging to this clan
        /// </summary>
        public virtual ICollection<ChoreRecord> Chores { get; set; }
    }

}