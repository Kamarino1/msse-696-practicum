﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ToDoWebApp.AppCode.Domain
{
    /// <summary>
    /// This class defines the User of the application. It extends
    /// IdentityUser to user to be used with ASP.Net Identity Framework
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// This static variable is meant to go between the Clan Name and the User Name
        /// to generate a unique username for Owin Authentication.Any changes to this variable
        /// will require the application users database to be updated so all user names reflect
        /// this change.DO NOT CHANGE WITHOUT UPDATING THE DATABASE RECORDS OR NO ONE WILL BE
        /// ABLE TO LOG IN
        /// </summary>
        public static string userNameDelimiter = "/";

        /// <summary>
        /// Create the UserIdentity and assign the claims
        /// </summary>
        /// <param name="manager">ASP.Net UserManager</param>
        /// <returns>ClaimsIdentity</returns>
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("Name", this.Name.ToString()));
            userIdentity.AddClaim(new Claim("ClanName", this.ClanName.ToString()));
            userIdentity.AddClaim(new Claim("Email", this.Email.ToString()));

            return userIdentity;
        }

        /// <summary>
        /// User's ClanName
        /// </summary>
        public string ClanName { get; set; }

        /// <summary>
        /// User's Name
        /// </summary>
        public String Name { get; set; }


        /// <summary>
        /// Nullable for first user entry, cannot use Identity to insert record into
        /// Clans database, must first insert user then insert ClanName record then
        /// link the two databases
        /// </summary>
        public int? ClanId { get; set; }

        /// <summary>
        /// The next two records are strictly used for the customUserValidator
        /// Cannot assign a user Role until user account is created
        /// and UserValidator validates the user information based on role
        /// This attribute is not stored in the database
        /// </summary>
        [NotMapped]
        public string Role { get; set; }


        /// <summary>
        /// If user is the primary parent (the first parent to have account created)
        /// need to validate that ClanName is unique (secondary parent would fail
        /// that validation)
        /// This attribute is not stored in the database
        /// </summary>
        [NotMapped]
        public bool PrimaryParent { get; set; }

        /// <summary>
        /// The Clan that this user belongs to
        /// </summary>
        public virtual Clan clan { get; set; }

        /// <summary>
        /// Chores that this parent user has submitted
        /// This is an inverse property to allow ChoreRecord to link to
        /// the ApplicationUser twice
        /// </summary>
        public virtual ICollection<ChoreRecord> ParentSubmittedChores { get; set; }

        /// <summary>
        /// Chores that this child user has submitted
        /// This is an inverse property to allow ChoreRecord to link to
        /// the ApplicationUser twice
        /// </summary>
        public virtual ICollection<ChoreRecord> ChildSubmittedChores { get; set; }
    }
}