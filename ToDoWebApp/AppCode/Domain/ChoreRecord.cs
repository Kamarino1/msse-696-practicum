﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ToDoWebApp.AppCode.Domain
{
    /// <summary>
    /// A data entity to hold a chore with associated reward data
    /// </summary>
    public class ChoreRecord
    {
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ChoreRecord() { }

        /// <summary>
        /// Constructor that takes 3 parameters, this constructor was used for testing
        /// and seeding the database
        /// </summary>
        /// <param name="child">Child who completed the chore</param>
        /// <param name="chore">The chore that was completed</param>
        /// <param name="reward">The reward assigned to this chore</param>
        public ChoreRecord(string child, string chore, string reward)
        {
            this.ChildId = child;
            this.Chore = chore;
            this.Reward = reward;
        }

        /// <summary>
        /// Chore Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id of the clan this chore belongs to
        /// </summary>
        public int ClanId { get; set; }
        
        /// <summary>
        /// The Chore that is to be completed
        /// </summary>
        public string Chore { get; set; }

        /// <summary>
        /// The Reward associated with completing this chore
        /// </summary>
        public string Reward { get; set; }

        /// <summary>
        /// The Id of the child that completed the chore
        /// </summary>
        public string ChildId { get; set; }

        /// <summary>
        /// The date that the chore was approved as complete by the parent
        /// Nullable
        /// </summary>
        public DateTime? ChoreComplete { get; set; }
        
        /// <summary>
        /// The date the chore was submitted as complete by the child
        /// Nullable
        /// </summary>
        public DateTime? ChoreSubmitted { get; set; }

        /// <summary>
        /// True if the chore has been approved by a parent
        /// </summary>
        public bool ChoreApproved { get; set; }
        
        /// <summary>
        /// The date the Reward was approved by a parent
        /// Nullable
        /// </summary>
        public DateTime? RewardComplete { get; set; }
        
        /// <summary>
        /// The date the Reward was submitted for redemption by a child
        /// Nullable
        /// </summary>
        public DateTime? RewardSubmitted { get; set; }

        /// <summary>
        /// True if the reward was submitted and approved
        /// </summary>
        public bool RewardApproved { get; set; }

        /// <summary>
        /// Date the Chore was added by a parent
        /// </summary>
        public DateTime DateSubmitted { get; set; }

        /// <summary>
        /// Id of Parent who added the chore
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// Clan that the chore belongs to
        /// </summary>
        public virtual Clan clan { get; set; }

        /// <summary>
        /// The parent Identity that added the chore
        /// </summary>
        [ForeignKey("ParentId")]
        [InverseProperty("ParentSubmittedChores")]
        public virtual ApplicationUser Parent { get; set; }

        /// <summary>
        /// The child Identity that submitted the chore
        /// </summary>
        [ForeignKey("ChildId")]
        [InverseProperty("ChildSubmittedChores")]
        public virtual ApplicationUser Child { get; set; }
    }
}