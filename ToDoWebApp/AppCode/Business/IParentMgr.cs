﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoWebApp.Models;

namespace ToDoWebApp.AppCode.Business
{
    /// <summary>
    /// This interface is strictly used with ASP.Net Core Dependency Injection.
    /// It defines the Parent Manager that defines the business logic for the Parent
    /// User Use Cases
    /// The manager Returns views that can be used by the controller by consuming
    /// domain objects that it receives from the implemented services
    /// </summary>
    public interface IParentMgr
    {
        /// <summary>
        /// Find a chore by its Id
        /// </summary>
        /// <param name="id">Integer of the Chore Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>EditChroeViewModel</returns>
        Task<EditChoreViewModel> getChoreById(int id, string userId);

        /// <summary>
        /// Adds a new chore to the chore store
        /// </summary>
        /// <param name="chore">AddChroeViewModel containing the chore to add</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        Task<ParentChoreTableViewModel> AddChore(AddChoreViewModel chore, string userId);

        /// <summary>
        /// Updates a chore in the chore store
        /// </summary>
        /// <param name="chore">EditChroeViewModel containing the updated chore information</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        Task<ParentChoreTableViewModel> EditChore(EditChoreViewModel chore, string userId);

        /// <summary>
        /// Removess a chore from the chore store
        /// </summary>
        /// <param name="id">Integer of the Chore Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        Task<ParentChoreTableViewModel> DeleteChore(int id, string userId);

        /// <summary>
        /// Approves a chore that was submitted as complete by a child user
        /// </summary>
        /// <param name="id">Integer of the Chore Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        Task<ParentChoreTableViewModel> ApproveSubmittedChore(int id, string userId);

        /// <summary>
        /// Denies a chore that was submitted as complete by a child user,
        /// this returns the chore to the awaiting completion list
        /// </summary>
        /// <param name="id">Integer of the Chore Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        Task<ParentChoreTableViewModel> DenySubmittedChore(int id, string userId);

        /// <summary>
        /// Approves a reward that was submitted as complete by a child user
        /// </summary>
        /// <param name="id">Integer of the Reward Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        Task<ParentChoreTableViewModel> ApproveSubmittedReward(int id, string userId);

        /// <summary>
        /// Denies a reward that was submitted for redemption by a child user,
        /// this returns the reward to the child's available rewards list
        /// </summary>
        /// <param name="id">Integer of the Reward Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        Task<ParentChoreTableViewModel> DenySubmittedReward(int id, string userId);

        /// <summary>
        /// Gets a list of all chores and rewards associated with the user's clan
        /// </summary>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>List of ParentHistoryViewModel</returns>
        Task<List<ParentHistoryViewModel>> GetHistory(string userId);

        /// <summary>
        /// Gets the view for the user making the request
        /// </summary>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        Task<ParentChoreTableViewModel> GetView(string userId);
    }
}