﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoWebApp.Models;
using ToDoWebApp.AppCode.Service;
using AutoMapper;
using ToDoWebApp.AppCode.Domain;
using System.Threading.Tasks;

namespace ToDoWebApp.AppCode.Business
{
    /// <summary>
    /// This class implements the IChildMgr interface
    /// The controller is responsible of implementing the role authentication
    /// This class utilizes ASP.Net Dependency Injection as well as homebrewed
    /// Dependency Injection as an exercise
    /// </summary>
    public class ChildMgr : IChildMgr
    {
        private IChoreSvc choreSvc;
        private IUserSvc _userSvc;
        private IMapper _mapper;

        /// <summary>
        /// Constructor that injects the IMapper implementation using
        /// ASP.Net Core Dependency Injection and chreates a choreSvc
        /// object using homebrewed DI
        /// </summary>
        /// <param name="mapper">IMapper implementation</param>
        /// <param name="userSvc">IUserSvc implementation</param>
        public ChildMgr(IMapper mapper, IUserSvc userSvc)
        {
            Factory factory = new Factory();
            choreSvc = (IChoreSvc)factory.GetService(typeof(IChoreSvc).Name);
            _mapper = mapper;
            _userSvc = userSvc;
        }

        /// <summary>
        /// Submits a chore as complete
        /// </summary>
        /// <param name="id">Integer that holds the chore Id</param>
        /// <param name="userId">User Id of the child who submitted the chore</param>
        /// <returns>ChildChoreTableViewModel</returns>
        public async Task<ChildChoreTableViewModel> SubmitChore(int id, string userId)
        {
            ApplicationUser user = await _userSvc.GetUserById(userId);

            //getChoreById has logic to verify clan Id, this next call verifies the
            //user is authorized to edit this record
            ChoreRecord record = getChoreById(id, user);
            try
            {
                record.ChoreSubmitted = DateTime.Today;
                record.ChildId = user.Id;
                choreSvc.UpdateChore(record);
            }
            catch { }//may get null reference from getChoreById(), just catch it and return the view to the user

            return GetView(user);
        }

        /// <summary>
        /// Removes a chore as being submitted and returns it to awaiting completion
        /// </summary>
        /// <param name="id">Integer that holds the chore Id</param>
        /// <param name="userId">User Id of the child who is removing the chore</param>
        /// <returns>ChildChoreTableViewModel</returns>
        public async Task<ChildChoreTableViewModel> RemoveChoreFromSubmittal(int id, string userId)
        {
            ApplicationUser user = await _userSvc.GetUserById(userId);

            //getChoreById has logic to verify clan Id, this next call verifies the
            //user is authorized to edit this record
            ChoreRecord record = getChoreById(id, user);
            try
            {
                record.ChoreSubmitted = null;
                record.ChildId = null;
                choreSvc.UpdateChore(record);
            }
            catch { }//may get null reference from getChoreById(), just catch it and return the view to the user

            return GetView(user);
        }

        /// <summary>
        /// Removes a reward as being submitted and returns it to the available list
        /// </summary>
        /// <param name="id">Integer that holds the chore Id</param>
        /// <param name="userId">User Id of the child who is removing the reward</param>
        /// <returns>ChildChoreTableViewModel</returns>
        public async Task<ChildChoreTableViewModel> RemoveRewardFromSubmittal(int id, string userId)
        {
            ApplicationUser user = await _userSvc.GetUserById(userId);

            //getChoreById has logic to verify clan Id, this next call verifies the
            //user is authorized to edit this record
            ChoreRecord record = getChoreById(id, user);
            try
            {
                record.RewardSubmitted = null;
                choreSvc.UpdateChore(record);
            }
            catch { }//may get null reference from getChoreById(), just catch it and return the view to the user

            return GetView(user);
        }

        /// <summary>
        /// Submits a reward for redemption
        /// </summary>
        /// <param name="id">Integer that holds the chore Id</param>
        /// <param name="userId">User Id of the child who submitted the reward for redemption</param>
        /// <returns>ChildChoreTableViewModel</returns>
        public async Task<ChildChoreTableViewModel> SubmitReward(int id, string userId)
        {
            ApplicationUser user = await _userSvc.GetUserById(userId);

            //getChoreById has logic to verify clan Id, this next call verifies the
            //user is authorized to edit this record
            ChoreRecord record = getChoreById(id, user);
            try
            {
                record.RewardSubmitted = DateTime.Today;
                choreSvc.UpdateChore(record);
            }
            catch { }//may get null reference from getChoreById(), just catch it and return the view to the user

            return GetView(user);
        }

        /// <summary>
        /// Gets the list of chores and rewards for the child user
        /// </summary>
        /// <param name="userId">User Id of the child who requested the view</param>
        /// <returns>ChildChoreTableViewModel</returns>
        public async Task<ChildChoreTableViewModel> GetView(string userId)
        {
            ApplicationUser user = await _userSvc.GetUserById(userId);

            return GetView(user);
        }

        /// <summary>
        /// Gets a list of all chores and rewards associated with the user
        /// </summary>
        /// <param name="userId">User Id of the associated user</param>
        /// <returns>List of ChildHistorViewModel</returns>
        public async Task<List<ChildHistoryViewModel>> GetHistory(string userId)
        {
            ApplicationUser user = await _userSvc.GetUserById(userId);

            return _mapper.Map < List<ChildHistoryViewModel> > 
                (choreSvc.GetChoresByClan((int)user.ClanId).Where(x => x.ChildId == user.Id).ToList());
        }

        // This method takes an application user and creates a view model
        // that will be returned to the controller
        private ChildChoreTableViewModel GetView(ApplicationUser user)
        {
            ChildChoreTableViewModel view = new ChildChoreTableViewModel();
            List<ChoreRecord> chores = choreSvc.GetChoresByClan((int)user.ClanId);

            view.ChoresAwaitingApproval = _mapper.Map<List<ChoreBase>>
                (chores.FindAll(x =>
                x.ChoreSubmitted != null &&
                x.ChoreApproved == false).ToList());

            view.ChoresAwaitingCompletion = _mapper.Map<List<ChoreBase>>
                (chores.FindAll(x => x.ChoreSubmitted == null).ToList());

            view.RewardsAwaitingAction = _mapper.Map<List<RewardBase>>
                (chores.FindAll(x =>
                x.ChoreApproved == true &&
                x.RewardApproved == false &&
                x.RewardSubmitted != null &&
                x.ChildId == user.Id).ToList());

            view.RewardsAvailable = _mapper.Map<List<RewardBase>>
                (chores.FindAll(x =>
                x.ChildId == user.Id &&
                x.ChoreApproved == true &&
                x.RewardSubmitted == null &&
                !string.IsNullOrWhiteSpace(x.Reward)).ToList());

            return view;
        }

        // This method returns a choreRecord using the chore ID. It verifies
        // that the chore belongs to the clan of the user that is making the request
        private ChoreRecord getChoreById(int id, ApplicationUser user)
        {
            ChoreRecord chore = choreSvc.getChoreById(id);
            if (user.ClanId == chore.ClanId)
                return chore;
            return null;
        }
    }
}