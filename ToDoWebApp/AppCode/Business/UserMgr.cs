﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ToDoWebApp.Models;
using System.Collections.Generic;
using ToDoWebApp.AppCode.Domain;
using ToDoWebApp.AppCode.Service;
using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Net.Mail;
using System.Web;

namespace ToDoWebApp.AppCode.Business
{
    /// <summary>
    /// This class implements the IUserMgr interface
    /// This class utilizes ASP.Net Dependency Injectione
    /// </summary>
    public class UserMgr : IUserMgr
    {
        private IUserSvc _userSvc;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor that injects the IMapper implementation  and
        /// the IUserSvc implementation using
        /// ASP.Net Core Dependency Injection
        /// </summary>
        /// <param name="mapper">IMapper implementation</param>
        /// <param name="userSvc">IUserSvc implementation</param>
        public UserMgr(IMapper mapper, IUserSvc userSvc)
        {
            _mapper = mapper;
            _userSvc = userSvc;
        }

        /// <summary>
        /// Registers a new family of users
        /// </summary>
        /// <param name="model">RegisterViewModel that contains all of the users'
        /// information to register</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        public async Task<IdentityResult> Register(RegisterViewModel model)
        {
            List<String> errors = new List<string>();
            bool success = true;

            ApplicationUser user = new ApplicationUser
            {
                UserName = model.ClanName + ApplicationUser.userNameDelimiter + model.UserName,
                Name = model.UserName,
                ClanName = model.ClanName,
                Email = model.Email,
                Role = "Parent",
                PrimaryParent = true
            };

            //Call service to create user
            IdentityResult result = await _userSvc.RegisterFirstUser(user, model.Password);

            success = success & result.Succeeded;
            errors.AddRange(result.Errors);

            if (success)
            {
                //Call service to add user to role and sign user in
                await _userSvc.AddToRole(user);
                await _userSvc.SignIn(user);

                if (model.SecondParent)
                {
                    ApplicationUser secondUser = new ApplicationUser
                    {
                        UserName = model.ClanName + ApplicationUser.userNameDelimiter + model.SecondUserName,
                        Name = model.SecondUserName,
                        Email = model.SecondEmail,
                        ClanName = model.ClanName,
                        ClanId = user.ClanId,
                        Role = "Parent",
                        PrimaryParent = false
                    };

                    //Call service to create user
                    IdentityResult result2 = await _userSvc.RegisterUser(secondUser, model.SecondPassword);
                    success = success & result2.Succeeded;
                    if (success)
                    {
                        await _userSvc.AddToRole(secondUser);
                    }
                    errors.AddRange(result2.Errors);
                }

                foreach (Child child in model.Children)
                {
                    //SignIn manager fails when email is Null. Store a white space to fool SignIn Manager
                    string email;
                    if (string.IsNullOrEmpty(child.Email))
                    {
                        email = " ";
                    }
                    else
                    {
                        email = child.Email;
                    }
                    ApplicationUser childUser = new ApplicationUser
                    {
                        UserName = model.ClanName + ApplicationUser.userNameDelimiter + child.UserName,
                        Name = child.UserName,
                        Email = email,
                        ClanName = model.ClanName,
                        ClanId = user.ClanId,
                        Role = "Child",
                        PrimaryParent = false
                    };

                    //Call service to create user
                    IdentityResult result3 = await _userSvc.RegisterUser(childUser, child.Password);
                    success = success & result3.Succeeded;
                    if (result3.Succeeded)
                    {
                        //Call service to add user to role
                        await _userSvc.AddToRole(childUser);
                    }
                    errors.AddRange(result3.Errors);
                }
            }
            else
            {
                errors.Add("FirstFailed");
            }

            if (errors.Count() > 0)
            {
                IdentityResult results = new IdentityResult(errors.ToArray());

                return results;
            }
            return IdentityResult.Success;
        }

        /// <summary>
        /// Checks if a clan already exists in the database
        /// </summary>
        /// <param name="name">Name of Clan to check</param>
        /// <returns>True if clan is already in Database</returns>
        public bool ClanExists(string name)
        {
            try
            {
                var users = _userSvc.GetUsersByClan(name);
                if (users != null && users.Count > 0)
                    return true;
                else
                    return false;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }

        /// <summary>
        /// Checks if an email address already exists in the User Store
        /// </summary>
        /// <param name="email"></param>
        /// <returns>True if Email is already in User Store</returns>
        public async Task<bool> EmailExists(string email)
        {
            if (await _userSvc.GetUserByEmail(email) != null)
                return true;
            return false;
        }

        /// <summary>
        /// Sign a user into the application
        /// </summary>
        /// <param name="clanName">Clan Name of the user</param>
        /// <param name="name">Name of the user</param>
        /// <param name="password">User's password</param>
        /// <returns>SignInStatus containing results of sign in attempt</returns>
        public async Task<SignInStatus> SignIn(string clanName, string name, string password)
        {
            if (ValidateUserLogin.Validate(clanName, name).Succeeded)
            {
                string userName = clanName + ApplicationUser.userNameDelimiter + name;

                return await _userSvc.SignIn(userName, password);
            }
            else
                return SignInStatus.Failure;
        }

        /// <summary>
        /// Sign a user into the application
        /// </summary>
        /// <param name="Id">String containing the User Id of user to sign in</param>
        /// <returns>Task</returns>
        public async Task SignIn(string Id)
        {
            ApplicationUser user = await _userSvc.GetUserById(Id);
            await _userSvc.SignIn(user);
        }

        /// <summary>
        /// Registers a new parent user to the given clan
        /// </summary>
        /// <param name="model">AddParentViewModel that contains all of the user's
        /// information to register</param>
        /// <param name="clanName">Clan Name of the Clan to add the new user to</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        public async Task<IdentityResult> RegisterParent(AddParentViewModel model, string clanName)
        {
            ApplicationUser user = new ApplicationUser
            {
                UserName = clanName + ApplicationUser.userNameDelimiter + model.UserName,
                Name = model.UserName,
                Email = model.Email,
                ClanName = clanName,
                Role = "Parent",
                PrimaryParent = false
            };

            //Call service to create user
            IdentityResult result = await _userSvc.RegisterUser(user, model.Password);

            if (result.Succeeded)
            {
                await _userSvc.AddToRole(user);
            }

            return result;
        }

        /// <summary>
        /// Registers a new child user to the given clan
        /// </summary>
        /// <param name="model">AddChildViewModel that contains all of the user's
        /// information to register</param>
        /// <param name="clanName">Clan Name of the Clan to add the new user to</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        public async Task<IdentityResult> RegisterChild(AddChildViewModel model, string clanName)
        {
            //SignIn manager fails when email is Null. Store a white space to fool SignIn Manager
            string email;
            if (string.IsNullOrEmpty(model.Email))
            {
                email = " ";
            }
            else
            {
                email = model.Email;
            }
            ApplicationUser user = new ApplicationUser
            {
                UserName = clanName + ApplicationUser.userNameDelimiter + model.UserName,
                Name = model.UserName,
                Email = email,
                ClanName = clanName,
                Role = "Child",
                PrimaryParent = false
            };

            //Call service to create user
            IdentityResult result = await _userSvc.RegisterUser(user, model.Password);

            if (result.Succeeded)
            {
                await _userSvc.AddToRole(user);
            }

            return result;
        }

        /// <summary>
        /// Returns user information for all users within the clan
        /// </summary>
        /// <param name="username">user name of user making the request</param>
        /// <param name="clanName">clan name of user</param>
        /// <returns>ManageViewModel containing all users within the clan</returns>
        public ManageViewModel GetFamilyUsers(string username, string clanName)
        {
            ManageViewModel view = new ManageViewModel();

            var users = _userSvc.GetUsersByClan(clanName);

            if (users != null && users.Count > 0)
            {
                //Map information of user making the request
                view = _mapper.Map<ManageViewModel>(users.First(x => x.UserName == username));

                //find the role Id for Parent and then extract parent users from list of users
                //This should be extracted into the service layer, why are you here?
                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
                var role = roleManager.FindByName("Parent").Users.First();
                view.Parents = _mapper.Map<List<User>>(users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.RoleId) && x.UserName != username).ToList());

                //Extract all users with Child role
                role = roleManager.FindByName("Child").Users.First();
                view.Children = _mapper.Map<List<User>>(users.Where(x => x.Roles.Select(y => y.RoleId).Contains(role.RoleId)).ToList());
            }

            return view;
        }

        /// <summary>
        /// Changes a user's password
        /// </summary>
        /// <param name="oldPass">String containing the old password</param>
        /// <param name="newPass">String containing the new password</param>
        /// <param name="userId">String containing the UserId of the User whose password will be updated</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        public async Task<IdentityResult> ChangePassword(string oldPass, string newPass, string userId)
        {
            var result = await _userSvc.UpdatePassword(oldPass, newPass, userId);
            if (result.Succeeded)
            {
                //now that we've changed the password, we need to re-sign in the user
                ApplicationUser user = await _userSvc.GetUserById(userId);
                if (user != null)
                {
                    await _userSvc.SignIn(user);
                }
                else
                {
                    return new IdentityResult("Encountered an unexpected error locating your account information! " +
                         "If this problem persists, please report this issue to our team.");
                }
            }

            return result;
        }

        /// <summary>
        /// Changes a user's password
        /// </summary>
        /// <param name="newPass">String containing the new password</param>
        /// <param name="clan">String containing the clan Name of the User whose password will be updated</param>
        /// <param name="name">String containing the User Name of the User whose password will be updated</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        public async Task<IdentityResult> ChangeChildPassword(string newPass, string clan, string name)
        {
            ApplicationUser user = await _userSvc.GetUserByName(clan + ApplicationUser.userNameDelimiter + name);
            if (user != null)
            {
                //cheat the system a bit by obtaining a new reset password token and resetting the child user's password
                string token = await _userSvc.getPassToken(user.Id);
                return await _userSvc.ResetPassword(token, newPass, user.Id);
            }
            else
            {
                return new IdentityResult("Encountered an unexpected error locating " + name +
                    "'s account information! If this problem persists, please report this issue to our team.");
            }
        }

        /// <summary>
        /// Resets a user's password that is used to log into the application
        /// </summary>
        /// <param name="email">email address of the user</param>
        /// <param name="token">Verification token used to confirm user's identity</param>
        /// <param name="password">new password to assign to the user</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        public async Task<IdentityResult> ResetPassword(string email, string token, string password)
        {
            ApplicationUser user = await _userSvc.GetUserByEmail(email);
            if (user != null)
            {
                var result = await _userSvc.ResetPassword(token, password, user.Id);
                //now that we've changed the password, we need to re-sign in the user
                if (result.Succeeded)
                {
                    await _userSvc.SignIn(user);
                }
                return result;
            }
            else
            {
                return new IdentityResult("We encountered an unexpected error trying to reset your password!");
            }
        }

        /// <summary>
        /// Updates the email on the Users account
        /// </summary>
        /// <param name="email">new email address</param>
        /// <param name="name">Name of User</param>
        /// <param name="clanName">Clan Name that user belongs to</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        public async Task<IdentityResult> UpdateEmail(string email, string name, string clanName)
        {
            ApplicationUser user = await _userSvc.GetUserByName(clanName + ApplicationUser.userNameDelimiter + name);
            List<string> errors = new List<string>();

            //verify user and email exist
            if (!string.IsNullOrWhiteSpace(email) && user != null)
            {
                //verify format
                try
                {
                    MailAddress m = new MailAddress(email);
                }
                catch (FormatException)
                {
                    errors.Add("The email address " + email + " is not the correct format!");
                }

                //verify email is unique
                ApplicationUser owner = await _userSvc.GetUserByEmail(email);
                if (owner != null && !EqualityComparer<string>.Default.Equals(owner.Id, user.Id))
                {
                    errors.Add("The email address " + email + " is already registered to another user!!");
                }
            }
            else if (user == null)
            {
                errors.Add("Encountered an unexpected error locating " + name + "'s account information! " +
                    "If this problem persists, please report this issue to our team.");
            }
            else
            {
                errors.Add("Email address is required");
            }
            //errors were encountered, return the errors
            if (errors.Count < 0)
            {
                return new IdentityResult(errors);
            }
            else //update user's email
            {
                user.Email = email;
                return await _userSvc.UpdateUser(user);
            }
        }

        /// <summary>
        /// Send a user a reset password token
        /// </summary>
        /// <param name="email">email to send token to</param>
        /// <param name="callBack">URL that the user should return to to reset password</param>
        /// <returns>Task</returns>
        public async Task SendForgotPasswordCode(string email, string callBack)
        {
            ApplicationUser user = await _userSvc.GetUserByEmail(email);
            if (user != null)
            {
                string code = await _userSvc.getPassToken(user.Id);

                //IMPORTANT, must encode the token to be URL safe, if this step is skipped, certain characters in the token will cause errors in the URL
                code = HttpUtility.UrlEncode(code);

                //append the User Id and Token to the URL, this will be required when the user posts back
                callBack = callBack + "?UserId=" + user.Id + "&code=" + code;

                //Another way to do this would be to create an email template and inject the unique information into it
                await _userSvc.SendEmail(user.Id, "ToDo.com Password Reset", "You can reset your password by following this link: <a href=\"" + callBack + "\">link</a>" +
                    System.Environment.NewLine + "If you did not request a password reset, please ignore this email.");
            }
            return;
        }

        /// <summary>
        /// Gets User Information to display during the password reset activity
        /// </summary>
        /// <param name="userId">User Id of user resetting their password</param>
        /// <returns>ResetPasswordViewModel</returns>
        public async Task<ResetPasswordViewModel> GetPasswordView(string userId)
        {
            ResetPasswordViewModel model = new ResetPasswordViewModel();

            //The reset password view is also used to retrieve forgotten clan and user names,
            //populate those fields for the view and return
            ApplicationUser user = await _userSvc.GetUserById(userId);
            if (user != null)
            {
                model.Clan = user.clan.ClanName;
                model.UserName = user.Name;
            }
            return model;
        }
    }
}