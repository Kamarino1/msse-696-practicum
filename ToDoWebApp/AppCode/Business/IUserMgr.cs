﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using ToDoWebApp.Models;

namespace ToDoWebApp.AppCode.Business
{
    /// <summary>
    /// This interface is strictly used with ASP.Net Core Dependency Injection.
    /// It defines the User Manager that defines the business logic for the Account
    /// and user management Use Cases
    /// </summary>
    public interface IUserMgr
    {
        /// <summary>
        /// Registers a new family of users
        /// </summary>
        /// <param name="model">RegisterViewModel that contains all of the users'
        /// information to register</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        Task<IdentityResult> Register(RegisterViewModel model);

        /// <summary>
        /// Registers a new child user to the given clan
        /// </summary>
        /// <param name="model">AddChildViewModel that contains all of the user's
        /// information to register</param>
        /// <param name="clanName">Clan Name of the Clan to add the new user to</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        Task<IdentityResult> RegisterChild(AddChildViewModel model, string clanName);

        /// <summary>
        /// Registers a new parent user to the given clan
        /// </summary>
        /// <param name="model">AddParentViewModel that contains all of the user's
        /// information to register</param>
        /// <param name="clanName">Clan Name of the Clan to add the new user to</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        Task<IdentityResult> RegisterParent(AddParentViewModel model, string clanName);

        /// <summary>
        /// Changes a user's password that is used to log into the application
        /// </summary>
        /// <param name="oldPass">String containing the old password</param>
        /// <param name="newPass">String containing the new password</param>
        /// <param name="userId">String containing the UserId of the User whose password will be updated</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        Task<IdentityResult> ChangePassword(string oldPass, string newPass, string userId);

        /// <summary>
        /// Changes a user's password that is used to log into the application
        /// </summary>
        /// <param name="newPass">String containing the new password</param>
        /// <param name="clan">String containing the Clan Name of the User whose password will be updated</param>
        /// <param name="name">String containing the User Name of the User whose password will be updated</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        Task<IdentityResult> ChangeChildPassword(string newPass, string clan, string name);

        /// <summary>
        /// Resets a user's password that is used to log into the application
        /// </summary>
        /// <param name="email">email address of the user</param>
        /// <param name="token">Verification token used to confirm user's identity</param>
        /// <param name="password">new password to assign to the user</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        Task<IdentityResult> ResetPassword(string email, string token, string password);

        /// <summary>
        /// Updates the email on the Users account
        /// </summary>
        /// <param name="email">new email address</param>
        /// <param name="name">Name of User</param>
        /// <param name="clanName">Clan Name that user belongs to</param>
        /// <returns>IdentityResult containing results of the transaction</returns>
        Task<IdentityResult> UpdateEmail(string email, string name, string clanName);

        /// <summary>
        /// Gets User Information to display during the password reset activity
        /// </summary>
        /// <param name="userId">User Id of user resetting their password</param>
        /// <returns>ResetPasswordViewModel</returns>
        Task<ResetPasswordViewModel> GetPasswordView(string userId);

        /// <summary>
        /// Sign a user into the application
        /// </summary>
        /// <param name="clanName">Clan Name of the user</param>
        /// <param name="name">Name of the user</param>
        /// <param name="password">User's password</param>
        /// <returns>SignInStatus containing results of sign in attempt</returns>
        Task<SignInStatus> SignIn(string clanName, string name, string password);

        /// <summary>
        /// Sign a user into the application
        /// </summary>
        /// <param name="Id">String containing the User Id of user to sign in</param>
        /// <returns>Task</returns>
        Task SignIn(string Id);

        /// <summary>
        /// Checks if an email address already exists in the User Store
        /// </summary>
        /// <param name="email"></param>
        /// <returns>True if Email is already in User Store</returns>
        Task<bool> EmailExists(string email);

        /// <summary>
        /// Send a user a reset password token
        /// </summary>
        /// <param name="email">email to send token to</param>
        /// <param name="callBack">URL that the user should return to to reset password</param>
        /// <returns>Task</returns>
        Task SendForgotPasswordCode(string email, string callBack);

        /// <summary>
        /// Returns user information for all users within the clan
        /// </summary>
        /// <param name="username">user name of user making the request</param>
        /// <param name="clanName">clan name of user</param>
        /// <returns>ManageViewModel containing all users within the clan</returns>
        ManageViewModel GetFamilyUsers(string username, string clanName);

        /// <summary>
        /// Checks if a clan already exists in the database
        /// </summary>
        /// <param name="name">Name of Clan to check</param>
        /// <returns>True if clan is already in Database</returns>
        bool ClanExists(string name);

    }
}