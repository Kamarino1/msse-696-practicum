﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoWebApp.Models;
using ToDoWebApp.AppCode.Service;
using AutoMapper;
using ToDoWebApp.AppCode.Domain;
using System.Threading.Tasks;

namespace ToDoWebApp.AppCode.Business
{
    /// <summary>
    /// This class implements the IParentMgr interface
    /// The controller is responsible of implementing the role authentication
    /// This class utilizes ASP.Net Dependency Injection as well as homebrewed
    /// Dependency Injection as an exercise
    /// </summary>
    public class ParentMgr : IParentMgr
    {
        private IChoreSvc choreSvc;
        private IMapper _mapper;
        private IUserSvc _userSvc;

        /// <summary>
        /// Constructor that injects the IMapper and IUserSvc implementations 
        /// using ASP.Net Core Dependency Injection and chreates a choreSvc
        /// object using homebrewed DI
        /// </summary>
        /// <param name="mapper">IMapper implementation</param>
        /// <param name="userSvc">IUserSvc implementation</param>
        public ParentMgr(IMapper mapper, IUserSvc userSvc)
        {
            Factory factory = new Factory();
            choreSvc = (IChoreSvc)factory.GetService(typeof(IChoreSvc).Name);
            _mapper = mapper;
            _userSvc = userSvc;
        }

        /// <summary>
        /// Find a chore by its Id
        /// </summary>
        /// <param name="id">Integer of the Chore Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>EditChroeViewModel</returns>
        public async Task<EditChoreViewModel> getChoreById(int id, string userId)
        {
            ApplicationUser user = await _userSvc.GetUserById(userId);

            ChoreRecord chore = choreSvc.getChoreById(id);
            if (user.ClanId == chore.ClanId)
            {
                return _mapper.Map<EditChoreViewModel>(chore);
            }

            return null;
        }

        /// <summary>
        /// Adds a new chore to the chore store
        /// </summary>
        /// <param name="chore">AddChroeViewModel containing the chore to add</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        public async Task<ParentChoreTableViewModel> AddChore(AddChoreViewModel chore, string userId)
        {
            ApplicationUser user = await _userSvc.GetUserById(userId);

            //manual mapping of ApplicationUser and AddChoreViewModel to a ChoreRecord
            //This isn't the best method of execution as it segregates itself from the 
            //rest of the mappings
            ChoreRecord record = new ChoreRecord();
            record.ClanId = (int)user.ClanId;
            record.ParentId = user.Id;
            record.DateSubmitted = DateTime.Today;
            record.Chore = chore.Chore;
            record.Reward = chore.Reward;

            choreSvc.AddChore(record);

            return await GetView(userId);
        }

        /// <summary>
        /// Updates a chore in the chore store
        /// </summary>
        /// <param name="chore">EditChroeViewModel containing the updated chore information</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        public async Task<ParentChoreTableViewModel> EditChore(EditChoreViewModel chore, string userId)
        {
            //getChoreById has logic to verify clan Id, this next call verifies the
            //user is authorized to edit this record
            ChoreRecord record = await getChoreRecordById(chore.Id, userId);
            try
            {
                record.Chore = chore.Chore;
                record.Reward = chore.Reward;
                choreSvc.UpdateChore(record);
            }
            catch
            {
                //may get null reference from getChoreById(), just catch it and return the view to the user
                //may also get a failure from updating the chore record
                //in some instances a better user experience may be to tell the user what went wrong, but 
                //this application is targeting children and users of all experience levels. It is believed 
                //that doing this may detract from the user experience as the targetted audience wouldn't 
                //know what to do with  that information
            }

            return await GetView(userId);
        }

        /// <summary>
        /// Removess a chore from the chore store
        /// </summary>
        /// <param name="id">Integer of the Chore Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        public async Task<ParentChoreTableViewModel> DeleteChore(int id, string userId)
        {
            //getChoreById has logic to verify clan Id, this next call verifies the
            //user is authorized to delete this record
            ChoreRecord record = await getChoreRecordById(id, userId);
            try
            {
                choreSvc.DeleteChore(record.Id);
            }
            catch { }// may get null reference from getChoreById(), just catch it and return the view to the user

            return await GetView(userId);
        }

        /// <summary>
        /// Approves a chore that was submitted as complete by a child user
        /// </summary>
        /// <param name="id">Integer of the Chore Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        public async Task<ParentChoreTableViewModel> ApproveSubmittedChore(int id, string userId)
        {
            //getChoreById has logic to verify clan Id, this next call verifies the
            //user is authorized to edit this record
            ChoreRecord record = await getChoreRecordById(id, userId);
            try
            {
                record.ChoreComplete = DateTime.Today;
                record.ChoreApproved = true;
                choreSvc.UpdateChore(record);
            }
            catch { }//may get null reference from getChoreById(), just catch it and return the view to the user

            return await GetView(userId);
        }

        /// <summary>
        /// Denies a chore that was submitted as complete by a child user,
        /// this returns the chore to the awaiting completion list
        /// </summary>
        /// <param name="id">Integer of the Chore Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        public async Task<ParentChoreTableViewModel> DenySubmittedChore(int id, string userId)
        {
            //getChoreById has logic to verify clan Id, this next call verifies the
            //user is authorized to edit this record
            ChoreRecord record = await getChoreRecordById(id, userId);
            try
            {
                record.ChoreSubmitted = null;
                record.ChildId = null;
                record.ChoreApproved = false;
                choreSvc.UpdateChore(record);
            }
            catch { }//may get null reference from getChoreById(), just catch it and return the view to the user

            return await GetView(userId);
        }

        /// <summary>
        /// Approves a reward that was submitted as complete by a child user
        /// </summary>
        /// <param name="id">Integer of the Reward Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        public async Task<ParentChoreTableViewModel> ApproveSubmittedReward(int id, string userId)
        {
            //getChoreById has logic to verify clan Id, this next call verifies the
            //user is authorized to edit this record
            ChoreRecord record = await getChoreRecordById(id, userId);
            try
            {
                record.RewardComplete = DateTime.Today;
                record.RewardApproved = true;
                choreSvc.UpdateChore(record);
            }
            catch { }//may get null reference from getChoreById(), just catch it and return the view to the user

            return await GetView(userId);
        }

        /// <summary>
        /// Denies a reward that was submitted for redemption by a child user,
        /// this returns the reward to the child's available rewards list
        /// </summary>
        /// <param name="id">Integer of the Reward Id</param>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        public async Task<ParentChoreTableViewModel> DenySubmittedReward(int id, string userId)
        {
            //getChoreById has logic to verify clan Id, this next call verifies the
            //user is authorized to edit this record
            ChoreRecord record = await getChoreRecordById(id, userId);
            try
            {
                record.RewardSubmitted = null;
                choreSvc.UpdateChore(record);
            }
            catch { }//may get null reference from getChoreById(), just catch it and return the view to the user

            return await GetView(userId);
        }

        /// <summary>
        /// Gets a list of all chores and rewards associated with the user's clan
        /// </summary>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>List of ParentHistoryViewModel</returns>
        public async Task<List<ParentHistoryViewModel>> GetHistory(string userId)
        {
            ApplicationUser user = await _userSvc.GetUserById(userId);

            List<ParentHistoryViewModel> view = _mapper.Map<List<ParentHistoryViewModel>>
                (choreSvc.GetChoresByClan((int)user.ClanId));
            return view;
        }

        //This method returns a chore based on its Id. This method has logic
        //to verify that the chore requested belongs to the user's clan who
        //made the request
        private async Task<ChoreRecord> getChoreRecordById(int id, string userId)
        {
            ApplicationUser user = await _userSvc.GetUserById(userId);

            ChoreRecord chore = choreSvc.getChoreById(id);
            if (user.ClanId == chore.ClanId)
                return chore;
            return null;
        }

        /// <summary>
        /// Gets the view for the user making the request
        /// </summary>
        /// <param name="userId">User Id of User Making request</param>
        /// <returns>ParentChoreTableViewModel</returns>
        public async Task<ParentChoreTableViewModel> GetView(string userId)
        {
            ApplicationUser user = await _userSvc.GetUserById(userId);

            //get a list of all chores associated with this user's clan
            List<ChoreRecord> chores = choreSvc.GetChoresByClan((int)user.ClanId);

            //execute the logic to segregate the list of chores into the appropriate categories
            //for display to the user
            ParentChoreTableViewModel view = new ParentChoreTableViewModel();
            view.ChoresAwaitingApproval = getChoresAwaitingApproval(chores);
            view.ChoresAwaitingCompletion = getChoresAwaitingCompletion(chores);
            view.RewardsAwaitingAction = getRewardsAwaitingApproval(chores);

            return view;
        }

        //This method takes a list of chores and defines the logic of what chores are awaiting completion
        private List<ChoresCompletion> getChoresAwaitingCompletion(List<ChoreRecord> chores)
        {
            return _mapper.Map<List<ChoresCompletion>>
                (chores.FindAll(x => x.ChoreSubmitted == null).ToList());
        }

        //This method takes a list of chores and defines the logic of what chores are awaiting approval
        private List<ChoresApproval> getChoresAwaitingApproval(List<ChoreRecord> chores)
        {
            return _mapper.Map<List<ChoresApproval>>
                (chores.FindAll(x => x.ChoreSubmitted != null &&
                x.ChoreApproved == false).ToList());
        }

        //This method takes a list of chores and defines the logic of what rewards are awaiting approval
        private List<RewardsApproval> getRewardsAwaitingApproval(List<ChoreRecord> chores)
        {
            return _mapper.Map<List<RewardsApproval>>
                (chores.FindAll
                    (x => x.ChoreApproved == true &&
                         x.RewardApproved == false &&
                         x.RewardSubmitted != null).ToList());
        }
    }
}