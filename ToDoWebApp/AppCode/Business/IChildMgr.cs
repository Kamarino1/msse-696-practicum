﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoWebApp.Models;

namespace ToDoWebApp.AppCode.Business
{
    /// <summary>
    /// This interface is strictly used with ASP.Net Core Dependency Injection.
    /// It defines the Child Manager that defines the business logic for the Child
    /// User Use Cases
    /// The manager Returns views that can be used by the controller by consuming
    /// domain objects that it receives from the implemented services
    /// </summary>
    public interface IChildMgr
    {
        /// <summary>
        /// Submits a chore as complete
        /// </summary>
        /// <param name="id">Integer that holds the chore Id</param>
        /// <param name="userId">User Id of the child who submitted the chore</param>
        /// <returns>ChildChoreTableViewModel</returns>
        Task<ChildChoreTableViewModel> SubmitChore(int id, string userId);

        /// <summary>
        /// Removes a chore as being submitted and returns it to awaiting completion
        /// </summary>
        /// <param name="id">Integer that holds the chore Id</param>
        /// <param name="userId">User Id of the child who is removing the chore</param>
        /// <returns>ChildChoreTableViewModel</returns>
        Task<ChildChoreTableViewModel> RemoveChoreFromSubmittal(int id, string userId);

        /// <summary>
        /// Removes a reward as being submitted and returns it to the available list
        /// </summary>
        /// <param name="id">Integer that holds the chore Id</param>
        /// <param name="userId">User Id of the child who is removing the reward</param>
        /// <returns>ChildChoreTableViewModel</returns>
        Task<ChildChoreTableViewModel> RemoveRewardFromSubmittal(int id, string userId);

        /// <summary>
        /// Submits a reward for redemption
        /// </summary>
        /// <param name="id">Integer that holds the chore Id</param>
        /// <param name="userId">User Id of the child who submitted the reward for redemption</param>
        /// <returns>ChildChoreTableViewModel</returns>
        Task<ChildChoreTableViewModel> SubmitReward(int id, string userId);

        /// <summary>
        /// Gets the list of chores and rewards for the child user
        /// </summary>
        /// <param name="userId">User Id of the child who requested the view</param>
        /// <returns>ChildChoreTableViewModel</returns>
        Task<ChildChoreTableViewModel> GetView(string userId);

        /// <summary>
        /// Gets a list of all chores and rewards associated with the user
        /// </summary>
        /// <param name="userId">User Id of the associated user</param>
        /// <returns>List of ChildHistorViewModel</returns>
        Task<List<ChildHistoryViewModel>> GetHistory(string userId);
    }
}