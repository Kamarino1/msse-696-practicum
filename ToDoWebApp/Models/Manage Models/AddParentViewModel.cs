﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for Adding a new Parent User
    /// </summary>
    public class AddParentViewModel
    {
        /// <summary>
        /// New Parent's User Name
        /// </summary>
        [Required]
        [Display(Name = "User name", Description =
            "Just like yours, the User Name for another user with Parental authorities can be as simple or as " +
            "complex as you want. However, may we suggest 'WorldsGreatestDad' or 'TheBestWifeAManCouldEverAskFor', " +
            "you know, for bonus points?")]
        [RegularExpression(@"^[A-Za-z0-9]*$", ErrorMessage = "Only letters and numbers are allowed " +
            "in User Name! Please enter a different User Name.")]
        public string UserName { get; set; }

        /// <summary>
        /// New Parent's Email
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address", Description =
            "We'll let the other Parental user be able to retrieve the account information as well. " +
            "So please enter an Email Address for them, but be sure to make it different from yours!")]
        [System.Web.Mvc.Remote("doesSecondEmailExist", "Account", HttpMethod = "POST",
            ErrorMessage = "Uh Oh, a user is already registered with this Email address." +
            "Please try a different Email address or go to Forgot Username/Password.")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "The entered Email Address is not a valid Email! " +
           "Please re-enter your Email Address and try again!")]
        public string Email { get; set; }

        /// <summary>
        /// Email Entry Confirmation
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Confirm Email", Description =
            "Please re-enter the other parental user's Email address to confirm it. " +
            "We aren't saying you can't type, just that a misake can happen at anytime!")]
        [Compare("Email", ErrorMessage = "The Email and confirmation Email do not match.")]
        public string ConfirmEmail { get; set; }

        /// <summary>
        /// New Parent's Password
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", Description =
            "Their password has to be 6 characters long, just like yours. Again, we aren't going to tell you " +
            "what needs to go in there, but we do suggest making it strong with a combination of letters " +
            "(both capital and lowercase), some digits, and maybe a special character or two.")]
        public string Password { get; set; }

        /// <summary>
        /// Password Entry Confirmation
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password", Description =
            "We really don't want you to mess this one up! We've been in hot water before and our " +
            "couch isn't that comfortable! Let's just make sure you actually typed what you think you did!")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}