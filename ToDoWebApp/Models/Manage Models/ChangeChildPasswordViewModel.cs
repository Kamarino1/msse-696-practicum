﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for Changing a Child's Password View
    /// </summary>
    public class ChangeChildPasswordViewModel
    {
        /// <summary>
        /// Name of Child whose password will be changed
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// New Password for Child
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", Description =
            "Your child's password should be easy for them to remember! But it is okay if they forget, as you " +
            "can come update it at anytime!")]
        public string Password { get; set; }

        /// <summary>
        /// New Password Entry Confirmation
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password", Description =
            "Hey, we just want to make sure that your child will be able to log in and start getting to work!")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}