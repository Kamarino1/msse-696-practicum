﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
/// <summary>
    /// View Model for the Child's Update Email View
    /// </summary>
    public class ChildUpdateEmailViewModel
    {
        /// <summary>
        /// Email Address
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        [System.Web.Mvc.Remote("doesEmailExist", "Account", HttpMethod = "POST",
            ErrorMessage = "User is already registered with this Email address." +
            "Please try a different Email address or go to Forgot Username/Password.")]
        [Display(Name = "Email Address", Description =
            "Don't worry about it! We will only use your email for retrieval of forgotten account information!")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "The entered Email Address is not a valid Email! " +
            "Please re-enter your Email Address and try again!")]
        public string Email { get; set; }

        /// <summary>
        /// Email entry confirmation
        /// </summary>
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Confirm Email", Description =
            "Please re-enter your Email address to confirm it...UHHH, we know! But hey, tpyos happen, right? " +
            "We just want to make sure we can get you your account information if you ever need it!")]
        [Compare("Email", ErrorMessage = "The Email and confirmation Email do not match.")]
        public string ConfirmEmail { get; set; }
    }
}