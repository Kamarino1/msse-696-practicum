﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for Manage View
    /// Extends User
    /// </summary>
    public class ManageViewModel : User
    {
        /// <summary>
        /// Constructor that initializes the list of parent and list of children users
        /// </summary>
        public ManageViewModel()
        {
            this.Children = new List<User>();
            this.Parents = new List<User>();
        }

        /// <summary>
        /// Clan Name
        /// </summary>
        [Display(Name = "Clan Name")]
        public string ClanName { get; set; }

        /// <summary>
        /// List of Children Users in Clan
        /// </summary>
        [Display(Name = "Children:")]
        public List<User> Children { get; set; }

        /// <summary>
        /// List of Other Parent Users in Clan
        /// </summary>
        [Display(Name = "Other Parent Users:")]
        public List<User> Parents { get; set; }
    }
}