﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// Base User Class to be extended/included by other classses
    /// </summary>
    public class User
    {
        /// <summary>
        /// User's User Name
        /// </summary>
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        /// <summary>
        /// User's Email Address
        /// </summary>
        [Display(Name = "Email Address")]
        public string Email { get; set; }
    }
}