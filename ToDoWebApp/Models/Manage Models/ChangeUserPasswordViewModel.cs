﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for Changing the Current User's password
    /// 
    /// 6 character password requirement
    /// </summary>
    public class ChangeUserPasswordViewModel
    {
        /// <summary>
        /// Current Password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current Password:", Description =
            "Enter your current password that you use to log into your account. That way we know it is you making" +
            " these changes and not someone else!")]
        public string OldPassword { get; set; }

        /// <summary>
        /// New Password
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New Password:", Description =
            "Your new password has to be 6 characters long. We aren't going to tell you what needs to go in there, but " +
            "we do suggest making it strong with a combination of letters (both capital and lowercase), some digits, " +
            "and maybe a special character or two.")]
        public string NewPassword { get; set; }

        /// <summary>
        /// New Password Entry Confirmation
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm New password", Description =
            "Let's put that new password in one more time just to make sure, what do you say?")]
        [Compare("NewPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}