﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for the Child's Manage View
    /// 
    /// Extends User
    /// </summary>
    public class ChildManageViewModel : User
    {
        /// <summary>
        /// Clan Name of Child
        /// </summary>
        [Display(Name = "Clan Name")]
        public string ClanName { get; set; }
    }
}