﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for the Child History View
    /// </summary>
    public class ChildHistoryViewModel
    {
        /// <summary>
        /// The Chore that was completed
        /// </summary>
        public string Chore { get; set; }

        /// <summary>
        /// The date the child submitted the reward for approval
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ChoreSubmitted { get; set; }

        /// <summary>
        /// The date the reward was approved by a parent
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ChoreComplete { get; set; }

        /// <summary>
        /// The reward associated with the chore
        /// </summary>
        public string Reward { get; set; }

        /// <summary>
        /// The date the child submitted the reward for redemption
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? RewardSubmitted { get; set; }

        /// <summary>
        /// Date the reward was satisified by the parent
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? RewardComplete { get; set; }
    }
}