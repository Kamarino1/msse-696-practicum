﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for the Child's Chore View
    /// </summary>
    public class ChildChoreTableViewModel
    {
        /// <summary>
        /// The Chores that still need to be done by the child
        /// </summary>
        [Display(Name = "Chores Awaiting Completion")]
        public List<ChoreBase> ChoresAwaitingCompletion { get; set; }

        /// <summary>
        /// The chores taht the child has submitted as complete and 
        /// are awaiting parental approval
        /// </summary>
        [Display(Name = "Chores Awaiting Approval")]
        public List<ChoreBase> ChoresAwaitingApproval { get; set; }

        /// <summary>
        /// The rewards that the child has submitted for parental approval
        /// </summary>
        [Display(Name = "Rewards Awaiting Approval")]
        public List<RewardBase> RewardsAwaitingAction { get; set; }

        /// <summary>
        /// The Rewards that the child has available to claim
        /// </summary>
        [Display(Name = "Rewards Available for Redemption")]
        public List<RewardBase> RewardsAvailable { get; set; }
    }
}