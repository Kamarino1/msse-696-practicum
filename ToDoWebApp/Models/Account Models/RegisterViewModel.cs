﻿using Foolproof;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for Register View
    /// </summary>
    public class RegisterViewModel
    {
        /// <summary>
        /// Constructor that initializes variables for default view
        /// </summary>
        public RegisterViewModel()
        {
            this.Children = new List<Child>();
            this.SecondParent = true;
            this.RememberMe = true;
        }

        /// <summary>
        /// Clan Name
        /// </summary>
        [Required]
        [Display(Name = "Clan Name", Description =
            "The Clan Name is a unique user name for your entire family. " +
            "Don't worry too much about it being complicated, we can remember it for you " +
            "so you and your children will only have to enter their unique user name within the clan!")]
        [System.Web.Mvc.Remote("doesClanNameExist", "Account", HttpMethod = "POST",
            ErrorMessage = "A family with this Clan Name already exists! Please try a different Clan Name.")]
        [RegularExpression(@"^[A-Za-z0-9]*$", ErrorMessage = "Only letters and numbers are allowed " +
            "in Clan Name! Please enter a different Clan Name.")]
        public string ClanName { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        [Required]
        [Display(Name = "User Name", Description =
            "Your User Name can be as simple as 'Mom' or 'Dad' or as complex as you want! " +
            "We use both your family's Clan name and your unique user name within that clan " +
            "to keep track of your account.")]
        [RegularExpression(@"^[A-Za-z0-9]*$", ErrorMessage = "Only letters and numbers are allowed " +
            "in User Name! Please enter a different User Name.")]
        public string UserName { get; set; }

        /// <summary>
        /// Email Address
        /// </summary>
        [Required]
        [DataType(DataType.EmailAddress)]
        [System.Web.Mvc.Remote("doesEmailExist", "Account", HttpMethod = "POST",
            ErrorMessage = "User is already registered with this Email address." +
            "Please try a different Email address or go to Forgot Username/Password.")]
        [Display(Name = "Email Address", Description =
            "Don't worry about it! We will only use your email for retrieval of forgotten account information!")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "The entered Email Address is not a valid Email! " +
            "Please re-enter your Email Address and try again!")]
        public string Email { get; set; }

        /// <summary>
        /// Confirm Email Address
        /// </summary>
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Confirm Email", Description =
            "Please re-enter your Email address to confirm it...UHHH, we know! But hey, tpyos happen, right? " +
            "We just want to make sure we can get you your account information if you ever need it!")]
        [Compare("Email", ErrorMessage = "The Email and confirmation Email do not match.")]
        public string ConfirmEmail { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", Description =
            "Your password has to be 6 characters long. We aren't going to tell you what needs to go in there, but " +
            "we do suggest making it strong with a combination of letters (both capital and lowercase), some digits, " +
            "and maybe a special character or two.")]
        public string Password { get; set; }

        /// <summary>
        /// Confirm Password
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password", Description =
            "Hey, just like your email up there, we want to make sure what you typed is what you think you typed! " +
            "Let's put that password in one more time just to make sure, what do you say?")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Number of children in the Clan
        /// </summary>
        [Required]
        [Display(Name = "Number of Children in Clan", Description =
            "How many children do you want to create an account for? You can add up to 10 " +
            "child accounts right now! Don't worry, if you need to add more now or later on " +
            "you can always add additional child accounts through the \"Manage Account\" page!")]
        public int? NumChildren { get; set; }

        /// <summary>
        /// Is there a second parent to add?
        /// </summary>
        [Display(Name = "Add Login information for a second Parent?", Description =
            "If you want, you can go ahead and enter in login information for a second user " +
            "with parental access! You will need to enter a User Name, Email, and Password for the " +
            "second parental user, but we'll do that in a minute.")]
        public bool SecondParent { get; set; }

        /// <summary>
        /// List of Children Users in clan
        /// </summary>
        public List<Child> Children { get; set; }

        /// <summary>
        /// Second Parent's UserName
        /// </summary>
        [RequiredIfTrue("SecondParent", ErrorMessage = "Second Parent's User Name is required!")]
        [Display(Name = "User name", Description =
            "Just like yours, the User Name for the second user with Parental authorities can be as simple or as " +
            "complex as you want. However, may we suggest 'WorldsGreatestDad' or 'TheBestWifeAManCouldEverAskFor', " +
            "you know, for bonus points?")]
        [RegularExpression(@"^[A-Za-z0-9]*$", ErrorMessage = "Only letters and numbers are allowed " +
            "in User Name! Please enter a different User Name.")]
        public string SecondUserName { get; set; }

        /// <summary>
        /// Second Parent's Email Address
        /// </summary>
        [RequiredIfTrue("SecondParent", ErrorMessage = "Second Parent's Email Address is required!")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address", Description =
            "We'll let the other Parental user be able to retrieve the account information as well. " +
            "So please enter an Email Address for them, but be sure to make it different from yours!")]
        [System.Web.Mvc.Remote("doesEmailExist", "Account", HttpMethod = "POST",
            ErrorMessage = "Uh Oh, a user is already registered with this Email address." +
            "Please try a different Email address or go to Forgot Username/Password.")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$", ErrorMessage = "The entered Email Address is not a valid Email! " +
           "Please re-enter your Email Address and try again!")]
        public string SecondEmail { get; set; }

        /// <summary>
        /// Confirm Email Address
        /// </summary>
        [RequiredIfTrue("SecondParent", ErrorMessage = "Email Address must be confirmed!")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Confirm Email", Description =
            "Please re-enter the other parental user's Email address to confirm it...UHHH, this again? " +
            "We aren't saying you can't type, just that a misake can happen at anytime!")]
        [Compare("SecondEmail", ErrorMessage = "The Email and confirmation Email do not match.")]
        public string SecondConfirmEmail { get; set; }

        /// <summary>
        /// Second Parent's Password
        /// </summary>
        [RequiredIfTrue("SecondParent", ErrorMessage = "Second Parent's Password is required!")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password", Description =
            "Their password has to be 6 characters long, just like yours. Again, we aren't going to tell you " +
            "what needs to go in there, but we do suggest making it strong with a combination of letters " +
            "(both capital and lowercase), some digits, and maybe a special character or two.")]
        public string SecondPassword { get; set; }

        /// <summary>
        /// Second Parent's Confirm Password
        /// </summary>
        [RequiredIfTrue("SecondParent", ErrorMessage = "Password must be confirmed!")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password", Description =
            "We really don't want you to mess this one up! We've been in hot water before and our " +
            "couch isn't that comfortable! Let's just make sure you actually typed what you think you did!")]
        [Compare("SecondPassword", ErrorMessage = "The password and confirmation password do not match.")]
        public string SecondConfirmPassword { get; set; }

        /// <summary>
        /// Remember Clan Name on next log in?
        /// </summary>
        [Display(Name = "Remember Clan Name?", Description =
            "We'll remember your Clan Name for you on this computer! That way, you and your children only " +
            "have to remember your unique user names and passwords! Please note that this feature requires " +
            "the use of Cookies and will not work if Cookies are disabled in your browser!")]
        public bool RememberMe { get; set; }
    }
}