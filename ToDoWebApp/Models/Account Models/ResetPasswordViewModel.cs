﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for Reset Password View
    /// </summary>
    public class ResetPasswordViewModel
    {
        /// <summary>
        /// User's Email Address
        /// </summary>
        [Required(ErrorMessage = "Your Email Address is required to reset your password!")]
        [EmailAddress(ErrorMessage = "The entered Email Address is not valid!")]
        [Display(Name = "Email:")]
        public string Email { get; set; }

        /// <summary>
        /// New Password
        /// </summary>
        [Required(ErrorMessage = "You must enter a new Password!")]
        [StringLength(100, ErrorMessage = "Your Password must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password:")]
        public string Password { get; set; }

        /// <summary>
        /// Password Entry Confirmation
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password:")]
        [Compare("Password", ErrorMessage = "The Password and Password Confirmation do not match.")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// Reset Password Token
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Clan Name
        /// </summary>
        [Display(Name = "Clan Name:")]
        public string Clan { get; set; }

        /// <summary>
        /// User Name
        /// </summary>
        [Display(Name = "User Name:")]
        public string UserName { get; set; }
    }
}