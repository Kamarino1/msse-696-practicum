﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for Forgot Passowrd
    /// </summary>
    public class ForgotPasswordViewModel
    {
        /// <summary>
        /// Email Address of user that forgot password
        /// </summary>
        [Required]
        [Display(Name = "Email Address:")]
        [EmailAddress(ErrorMessage = "The entered Email Address is not a valid Email! " +
            "Please re-enter your Email Address and try again!")]
        public string Email { get; set; }
    }
}
