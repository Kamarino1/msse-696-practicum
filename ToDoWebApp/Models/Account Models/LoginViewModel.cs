﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for Login View
    /// </summary>
    public class LoginViewModel
    {
        /// <summary>
        /// Clan name of User to Login
        /// </summary>
        [Required]
        [Display(Name = "Clan Name")]
        public string ClanName { get; set; }

        /// <summary>
        /// Name of user to log in
        /// </summary>
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        /// <summary>
        /// User's Password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// True of user wants to prepopulate Clan Name on next Log in
        /// </summary>
        [Display(Name = "Remember Clan Name?")]
        public bool RememberMe { get; set; }
    }
}