﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View model for Parent History
    /// </summary>
    public class ParentHistoryViewModel
    {
        /// <summary>
        /// Date chore was added to the system
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DateSubmitted{ get; set; }

        /// <summary>
        /// Child who completed the chore
        /// </summary>
        public string ChildName { get; set; }

        /// <summary>
        /// Chore
        /// </summary>
        public string Chore { get; set; }

        /// <summary>
        /// Date the chore was submitted as completed by the child
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ChoreSubmitted { get; set; }

        /// <summary>
        /// Date that a parent approved the chore as complete
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? ChoreComplete { get; set; }
 
        /// <summary>
        /// Reward Associated with the chore
        /// </summary>
        public string Reward { get; set; }

        /// <summary>
        /// Date the reward was submitted for redemption by the child
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? RewardSubmitted { get; set; }

        /// <summary>
        /// Date the reward was fulfilled by a parent
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? RewardComplete { get; set; }

        /// <summary>
        /// Parent who added the chore
        /// </summary>
        public string ParentName { get; set; }
    }
}