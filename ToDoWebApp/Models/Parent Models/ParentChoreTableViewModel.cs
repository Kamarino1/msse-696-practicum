﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View Model for the Parent Index
    /// </summary>
    public class ParentChoreTableViewModel
    {
        /// <summary>
        /// List of Chores that are still awaiting to be done
        /// </summary>
        [Display(Name = "Chores Awaiting Completion")]
        public List<ChoresCompletion> ChoresAwaitingCompletion { get; set; }

        /// <summary>
        /// Chore submitted by a child but are still awaiting parental approval
        /// </summary>
        [Display(Name = "Chores Awaiting Approval")]
        public List<ChoresApproval> ChoresAwaitingApproval { get; set; }

        /// <summary>
        /// Rewards submitted for redemption by a child but are awaiting parental fulfillment
        /// </summary>
        [Display(Name = "Rewards Awaiting Approval")]
        public List<RewardsApproval> RewardsAwaitingAction { get; set; }
    }

    /// <summary>
    /// Chores awaiting completion
    /// 
    /// Extends ChoreBase
    /// </summary>
    public class ChoresCompletion : ChoreBase
    {
    }

    /// <summary>
    /// Chores awaiting approval by a parent
    /// 
    /// Extends ChoreBase
    /// </summary>
    public class ChoresApproval : ChoreBase
    {
        /// <summary>
        /// Child who submitted chore
        /// </summary>
        [Display(Name = "Child")]
        public string ChildName { get; set; }
    }

    /// <summary>
    /// Rewards awaiting fulfillment by parent
    /// 
    /// Extends RewardBase
    /// </summary>
    public class RewardsApproval : RewardBase
    {
        /// <summary>
        /// Child who submitted reward
        /// </summary>
        [Display(Name = "Child")]
        public string ChildName { get; set; }
    }
}