﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// View model for Adding a new chore
    /// </summary>
    public class AddChoreViewModel
    {
        /// <summary>
        /// Chore
        /// </summary>
        [Required]
        [Display(Name = "Chore")]
        public string Chore { get; set; }

        /// <summary>
        /// Reward for completing the chore
        /// </summary>
        [Display(Name = "Reward")]
        public string Reward { get; set; }
    }
}