﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// Base Chore class to be extended/included by other classes
    /// </summary>
    public class ChoreBase
    {
        /// <summary>
        /// The Chore Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// The Chore Text
        /// </summary>
        [Required]
        [Display(Name = "Chore")]
        public string Chore { get; set; }

        /// <summary>
        /// The Reward Associated with this chore
        /// </summary>
        [Display(Name = "Reward")]
        public string Reward { get; set; }
    }
}