﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// Base Child Class to be Extended/Included by other classes
    /// </summary>
    public class Child
    {
        /// <summary>
        /// Child's User Name
        /// </summary>
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        /// <summary>
        /// Child's Email
        /// </summary>
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email (Optional)")]
        public string Email { get; set; }

        /// <summary>
        /// Child's Password
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Password Entry Confirmation
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
}