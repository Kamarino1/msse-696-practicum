﻿using System.ComponentModel.DataAnnotations;

namespace ToDoWebApp.Models
{
    /// <summary>
    /// Base Reward Class to be Extended by other classes
    /// </summary>
    public class RewardBase
    {
        /// <summary>
        /// Reward ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Reward Text
        /// </summary>
        [Display(Name = "Reward")]
        public string Reward { get; set; }
    }
}