﻿var previousSelection = 0;

$(function () {
    // Set up tooltips when text box has focus
    $(".form-control").tooltip({
        position: "center right",
        offset: [-2, 10],
        effect: "fade",
        opacity: 0.7
    });

    var addSecondParent;

    // Show or hide second parent (this is for a form reload, hiding the second parent if the checkbox
    // was unchecked and the form reloaded
    if ($("#SecondParent").is(":checked")) {
        $("#secondParentDiv").show();
        addSecondParent = true;
    } else {
        $("#secondParentDiv").hide();
        addSecondParent = false;
    }


    $(".wizard").first().fadeIn(0); // show first step

    // attach backStep button handler
    // hide on first step
    $("#back").hide().click(function () {
        var $step = $(".wizard:visible"); // get current step
        if ($step.prev().hasClass("wizard")) { // is there any previous step?
            $step.hide().prev().fadeIn(0);  // show it and hide current step

            $("#next").val("Next >"); //set next button back to Next >

            // disable backstep button?
            if (!$step.prev().prev().hasClass("wizard")) {
                $("#back").hide();
            }
        }
    });

    // attach nextStep button handler
    $("#next").click(function () {

        var $step = $(".wizard:visible"); // get current step
        var validator = $("form").validate(); // obtain validator
        var anyError = false;

        anyError = DropdownInvalid($("#NumChildren")); // validate dropdown



        $step.find("input").each(function () {
            if (!validator.element(this)) { // validate every input element inside this step
                anyError = true;
            }
        });

        if (anyError)
            return false; // exit if any error found

        if ($step.next().hasClass("wizard")) { // is there any next step?
            if (!$step.next().next().hasClass("wizard")) { // is next step the last?
                if (!addSecondParent) { // last step isn't being performed, submit the form
                    $("form").submit();
                }
                $("#next").val("Submit"); // change Next > to Submit
            }
            else {
                if ($step.next().next().find("#secondParentDiv").length && !addSecondParent) { // last step isn't being performed
                    $("#next").val("Submit"); // change Next > to Submit
                }
                //next step is the second step, draw the child information input boxes
                DrawChildInput();

                //reload validator for the dynamic content that was just drawn
                ValidateDynamicContent($("#children"));
            }
            $step.hide().next().fadeIn(0);  // show it and hide current step
            $("#back").show();   // recall to show backStep button
        }
        else { // this is last step, submit form
            $("form").submit();
        }
    });

    // Validate dropdown selection on change
    $("#NumChildren").change(function () {
        DropdownInvalid($('#NumChildren'));
    });

    // show or hide second parent input based on check box selection
    $("#SecondParent").click(function () {
        if ($(this).is(":checked")) {
            $("#secondParentDiv").show();
            addSecondParent = true;
        } else {
            $("#secondParentDiv").hide();
            addSecondParent = false;
        }
    });
});

// force validator to reload to validate children input section
function ValidateDynamicContent(element) {
    var currForm = element.closest("form");
    currForm.removeData("validator");
    currForm.removeData("unobtrusiveValidation");
    $.validator.unobtrusive.parse(currForm);
    currForm.validate().settings.ignore = ":hidden";
}

// add validation (before form submit) to the dropdown list for NumChildren
function DropdownInvalid(ddlId) {
    var ddl = ddlId.val();

    if (ddl !== '') {
        $("#NumChildrenError").hide();
        return false;
    } else {
        $("#NumChildrenError").show();
        return true;
    }

}

// Draw the input for the selected number of children
function DrawChildInput() {
    var numKids = $("#NumChildren").val();

    if (parseInt(numKids) < 10) {
        for (var i = parseInt(numKids); i < 10; i++) {
            $("#child" + i).detach();
        }
    }

    if (parseInt(previousSelection) < 10) {
        for (var ix = parseInt(previousSelection); ix < numKids; ix++) {
            DrawChildForm(ix);
        }
    }
    previousSelection = numKids;
}

// Child input formmatting
function DrawChildForm(index) {
    var displayKids = parseInt(index) + 1;
    var div = $("<div>", { id: "child" + index, "class": "a" });
    var header = $(document.createElement('h5')).html('Enter information for child number ' + displayKids);
    var userName = $(document.createElement('div')).attr("class", 'form-group');
    var email = $(document.createElement('div')).attr("class", 'form-group');
    var password = $(document.createElement('div')).attr("class", 'form-group');
    var confirmPass = $(document.createElement('div')).attr("class", 'form-group');
    var regex = "^([a-zA-Z0-9_\\-\\.]+)@@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";

    userName.after().html('<label class="col-md-2 control-label" for="Children_' + index + '__UserName">User name</label>' +
    '<div class="col-md-10">' +
        '<input class="form-control" data-val="true" data-val-regex="Only letters and numbers are allowed in User Name! Please enter a different User Name." data-val-regex-pattern="^[A-Za-z0-9]*$" data-val-required="The User name field is required." id="Children_' + index + '__UserName" name="Children[' + index + '].UserName" title="Feel free to use your children&#39;s real name here, as we will use the Clan Name in conjunction with their User Name to log them in!" type="text" value="" />' +
        '<span class="field-validation-valid text-danger" data-valmsg-for="Children[' + index + '].UserName" data-valmsg-replace="true"></span>' +
    '</div>');

    email.after().html('<label class="col-md-2 control-label" for="Children_' + index + '__Email">Email (Optional)</label>' +
    '<div class="col-md-10">' +
        '<input class="form-control" data-val="true" data-val-regex="The entered Email Address is not a valid Email! Please re-enter your Email Address and try again!" data-val-regex-pattern="' + regex + '" id="Children_' + index + '__Email" name="Children[' + index + '].Email" title="We don&#39;t need your child&#39;s email. The email can be used to send them updates when you add or approve things, but feel free to just yell down the hall to let them know!" type="text" value="" />' +
        '<span class="field-validation-valid text-danger" data-valmsg-for="Children[' + index + '].Email" data-valmsg-replace="true"></span>' +
    '</div>');

    password.after().html('<label class="col-md-2 control-label" for="Children_' + index + '__Password">Password</label>' +
    '<div class="col-md-10">' +
        '<input class="form-control" data-val="true" data-val-length="The Password must be at least 4 characters long." data-val-length-max="100" data-val-length-min="4" data-val-required="The Password field is required." id="Children_' + index + '__Password" name="Children[' + index + '].Password" title="Your child&#39;s password has to be 4 characters long. Try to keep it simple for them, that way they can log in and make sure the living room is swept!" type="password" />' +
        '<span class="field-validation-valid text-danger" data-valmsg-for="Children[' + index + '].Password" data-valmsg-replace="true"></span>' +
    '</div>');

    confirmPass.after().html('<label class="col-md-2 control-label" for="Children_' + index + '__ConfirmPassword">Confirm password</label>' +
    '<div class="col-md-10">' +
        '<input class="form-control" data-val="true" data-val-equalto="The password and confirmation password do not match." data-val-equalto-other="*.Password" id="Children_' + index + '__ConfirmPassword" name="Children[' + index + '].ConfirmPassword" title="Let&#39;s just make sure you entered that password correct! We want your kids to be able to log in and start cleaning, don&#39;t you?" type="password" />' +
        '<span class="field-validation-valid text-danger" data-valmsg-for="Children[' + index + '].ConfirmPassword" data-valmsg-replace="true"></span>' +
    '</div>');

    div.appendTo('#children');
    header.appendTo(div);
    userName.appendTo(div);
    email.appendTo(div);
    password.appendTo(div);
    confirmPass.appendTo(div);

    //set up tooltips for the childrens
    //Not the best method, as it makes numerous calls for each child, however, this is the
    //only method that would work with the on focus method
    $("#Children_" + index + "__UserName").tooltip({
        trigger: "focus",
        position: "center right",
        offset: [-2, 10],
        effect: "fade",
        opacity: 0.7
    });
    $("#Children_" + index + "__Email").tooltip({
        trigger: "focus",
        position: "center right",
        offset: [-2, 10],
        effect: "fade",
        opacity: 0.7
    });
    $("#Children_" + index + "__Password").tooltip({
        trigger: "focus",
        position: "center right",
        offset: [-2, 10],
        effect: "fade",
        opacity: 0.7
    });
    $("#Children_" + index + "__ConfirmPassword").tooltip({
        trigger: "focus",
        position: "center right",
        offset: [-2, 10],
        effect: "fade",
        opacity: 0.7
    });
}