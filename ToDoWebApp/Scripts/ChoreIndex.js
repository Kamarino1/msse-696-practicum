﻿var caaHidden = true;
var raaHidden = true;
var cacHidden = true;
var raHidden = true;

$(document).ready(function () {
    $(".content").hide();

    //One of the expand arrows was clicked, show or hide that section
    //and store the current state. The state must be stored so we
    //can re-apply the cuurent state after the partial view is reloaded
    $("#table-content").on("click", ".expand", function (event) {
        var id = $(this).data("id");
        $("i", this).toggleClass("fa-caret-down fa-caret-up");
        var content = ".content[data-id=" + id + "]";
        if ($(content).is(":visible")) {
            $(content).css("display", "none");
            switch (id) {
                case "caa":
                    caaHidden = true;
                    break;
                case "raa":
                    raaHidden = true;
                    break;
                case "cac":
                    cacHidden = true;
                    break;
                case "ra":
                    raHidden = true;
                    break;
            }
        }
        else {
            $(content).show();
            switch (id) {
                case "caa":
                    caaHidden = false;
                    break;
                case "raa":
                    raaHidden = false;
                    break;
                case "cac":
                    cacHidden = false;
                    break;
                case "ra":
                    raHidden = false;
                    break;
            }
        }
    });

    //this is to handle to modal "on success" in the ajax form
    $(".modal-content").on("click", ".close", function (event) {
        successFunction();
    });
});

//hide or show data according to previous state and
//set the direction of the expand arrow to match
function successFunction(sender, args) {
    if (caaHidden)
        $(".content[data-id=caa]").hide();
    else
        $("i", ".expand[data-id=caa]").attr("class", "fa fa-caret-up");
    if (raaHidden)
        $(".content[data-id=raa]").hide();
    else
        $("i", ".expand[data-id=raa]").attr("class", "fa fa-caret-up");
    if (cacHidden)
        $(".content[data-id=cac]").hide();
    else
        $("i", ".expand[data-id=cac]").attr("class", "fa fa-caret-up");
    if (raHidden)
        $(".content[data-id=ra]").hide();
    else
        $("i", ".expand[data-id=ra]").attr("class", "fa fa-caret-up");
}