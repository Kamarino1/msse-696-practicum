﻿//watch the container and load the partial view into the proper
//modal when the button is clicked.
//Note: it is important to watch the outermost container so
//a button that is loaded in a partial view will trigger this event
$(".container").on("click", ".loadModal", function (event) {
    var id = $(this).data("id");
    event.preventDefault();
    $.get(this.href, function (response) {
        $(".divForDisplay[data-id=" + id + "]").html(response);
    });
    $(".modal[data-id=" + id + "]").modal({
        backdrop: "static",
    }, "show");
});

//Change the buttons on form submit
$(".container").on("submit", "#modalForm", function (event) {
    if ($("form#modalForm").valid()) {
        // if form is valid, walk through each button and set value based on ID
        //it is necessary to walk through each button because if a modal is loaded,
        //it will stay loaded even though it isn't visible, so if another modal is
        //is loaded and it is after the previously loaded modal, #saveBtn will 
        //refer to the first modal and not the current modal
        $(".btn").each(function (i, obj) {
            if ($(this).is("#saveBtn")) {
                $(this).html("Working...");
                $(this).attr("disabled", "disabled")
            }
            if ($(this).is("#cancelBtn")) {
                $(this).hide();
            }
        });
    }
});

//Reload client side validation on the dynamically created content
function reloadValidation() {
    $("form#modalForm").ready(function () {
        $("form#modalForm").removeData("validator");
        $("form#modalForm").removeData("unobtrusiveValidation");
        $.validator.unobtrusive.parse($("form#modalForm"));
    });
}


