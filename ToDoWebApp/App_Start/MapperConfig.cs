﻿using AutoMapper;
using System;
using ToDoWebApp.Models;

namespace ToDoWebApp
{
    /// <summary>
    /// This class contains the configuration for AutoMapper
    /// </summary>
    public class MapperConfig : Profile
    {
        /// <summary>
        /// This class configures the maps between objects for automapper
        /// The Configure class is obsolete, however, using the current 
        /// method to configure a mapper profile did not work with ASP.Net
        /// Core Dependency injection. For this reason, the obsolete method,
        /// which does work, was chosen
        /// </summary>
        [Obsolete]
        protected override void Configure()
        {
            CreateMap<AppCode.Domain.ChoreRecord, ChoreBase>();
            CreateMap<AppCode.Domain.ChoreRecord, RewardBase>();
            CreateMap<AppCode.Domain.ChoreRecord, EditChoreViewModel>();
            CreateMap<AppCode.Domain.ChoreRecord, ParentHistoryViewModel>()
                .ForMember(dest => dest.ChildName, opt => opt.MapFrom(src => src.Child.Name))
                .ForMember(dest => dest.ParentName, opt => opt.MapFrom(src => src.Parent.Name));
            CreateMap<AppCode.Domain.ChoreRecord, ChoresCompletion>();
            CreateMap<AppCode.Domain.ChoreRecord, ChoresApproval>()
                .ForMember(dest => dest.ChildName, opt => opt.MapFrom(src => src.Child.Name));
            CreateMap<AppCode.Domain.ChoreRecord, RewardsApproval>()
                .ForMember(dest => dest.ChildName, opt => opt.MapFrom(src => src.Child.Name));
            CreateMap<AppCode.Domain.ChoreRecord, ChildHistoryViewModel>();
            CreateMap<AppCode.Domain.ApplicationUser, User>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name));
            CreateMap<AppCode.Domain.ApplicationUser, ManageViewModel>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Name));
        }
    }
}