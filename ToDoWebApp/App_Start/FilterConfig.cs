﻿using System.Web.Mvc;

namespace ToDoWebApp
{
    /// <summary>
    /// Configure the Filters used by the application
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// This method registers the filters that the application will use
        /// This application does not currently use the filters. A custom filter
        /// for error handling would be a beneficial addition to the application
        /// </summary>
        /// <param name="filters">A GlobalFilterCollection that holds the filters</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
