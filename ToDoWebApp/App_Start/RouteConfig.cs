﻿using System.Web.Mvc;
using System.Web.Routing;

namespace ToDoWebApp
{
    /// <summary>
    /// This class configures the routes used by the application
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// This method configures the routes that will be used. This application
        /// uses the default routes.
        /// </summary>
        /// <param name="routes">A RouteCollection that contains all of the routes used by the application</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
