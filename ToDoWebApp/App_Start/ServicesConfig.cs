﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Web.Mvc;
using ToDoWebApp.AppCode.Business;
using ToDoWebApp.AppCode.Service;
using ToDoWebApp.Extensions;

namespace ToDoWebApp
{
    /// <summary>
    /// This class configures the Services that will be used by ASP.Net Core Dependency Injection
    /// </summary>
    public class ServicesConfig
    {
        /// <summary>
        /// This method registers all of the service implementations that will be injected during runtime.
        /// This method relies on an extension called AddControllerAsService
        /// </summary>
        /// <param name="services">IServiceCollection that contains the service type and its implementation</param>
        public static void ConfigureServices(IServiceCollection services)
        {
            //Set up AutoMapper to work with DI, first call the configure method to configure it
            //then add it as a singleton
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MapperConfig());
            });
            services.AddSingleton(sp => config.CreateMapper());

            //add the controllers as a service, so they are added to the DI container
            services.AddControllersAsServices(typeof(Startup).Assembly.GetExportedTypes()
               .Where(t => !t.IsAbstract && !t.IsGenericTypeDefinition)
               .Where(t => typeof(IController).IsAssignableFrom(t)
                || t.Name.EndsWith("Controller", StringComparison.OrdinalIgnoreCase)));

            //The Managers must be added as a service. The Controller
            //has a dependency on the Manager, and the manager has a 
            //dependency on the service, to make this link work the DI
            //container must have all of these parts
            services.AddTransient<IParentMgr, ParentMgr>();
            services.AddTransient<IChildMgr, ChildMgr>();
            services.AddTransient<IUserMgr, UserMgr>();
            services.AddTransient<IUserSvc, UserSvcIdentityImpl>();
        }
    }
}