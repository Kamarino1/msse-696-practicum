﻿using System;
using SendGrid.Helpers.Mail;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;

namespace ToDoWebApp
{
    /// <summary>
    /// The class registers the email service api that will handle sending emails for the application
    /// </summary>
    public class EmailService : IIdentityMessageService
    {
        /// <summary>
        /// Send an Email
        /// </summary>
        /// <param name="message">An IdentityMessage that contains the email message to send</param>
        /// <returns>Task</returns>
        public Task SendAsync(IdentityMessage message)
        {
            return configSendGridasync(message);
        }

        // Configure SendGrid and send the email message
        private async Task configSendGridasync(IdentityMessage message)
        {
            //Remove the SendGrid API key from the code and place in a editable file, ie.
            // the web config file or in Azure so if it changes, we can update it
            dynamic sg = new SendGrid.SendGridAPIClient("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", "https://api.sendgrid.com");

            // Configure the email message to send using the SendGrid mail helpers
            Email from = new Email("accounts@ToDo.com");
            String subject = message.Subject;
            Email to = new Email(message.Destination);
            Content content = new Content("text/html", message.Body);
            Mail mail = new Mail(from, subject, to, content);

            dynamic response = await sg.client.mail.send.post(requestBody: mail.Get());
        }
    }
}
