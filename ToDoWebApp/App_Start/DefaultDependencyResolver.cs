﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Web.Mvc;
using System.Collections.Generic;

namespace ToDoWebApp
{
    /// <summary>
    /// This class defines the dependency resolver for the ASP.Net Core Dependency Injection
    /// </summary>
    public class DefaultDependencyResolver : IDependencyResolver
    {
        /// <summary>
        /// This is the service provider or the dependency resolver
        /// </summary>
        protected IServiceProvider serviceProvider;

        /// <summary>
        /// Constructor that sets the service provider
        /// </summary>
        /// <param name="serviceProvider">IServiceProvider</param>
        public DefaultDependencyResolver(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Returns a service based on the service type
        /// </summary>
        /// <param name="serviceType">a Type that represents the service type to be returned</param>
        /// <returns>Returns a service implementation as an Object</returns>
        public object GetService(Type serviceType)
        {
            return this.serviceProvider.GetService(serviceType);
        }

        /// <summary>
        /// Returns a group of services based on the service type
        /// </summary>
        /// <param name="serviceType">a Type that represents the service type to be returned</param>
        /// <returns>Returns service implementations as an IEnumerable Object</returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return this.serviceProvider.GetServices(serviceType);
        }

        IEnumerable<object> IDependencyResolver.GetServices(Type serviceType)
        {
            return this.serviceProvider.GetServices(serviceType);
        }
    }
}