﻿using System.Web.Optimization;

namespace ToDoWebApp
{
    /// <summary>
    /// BundleConfig contains one method that configures the script and sytle bundles for the views
    /// </summary>
    public class BundleConfig
    {
        /// <summary>
        /// RegisterBundles registers a number of bundles of scripts and styles that are commonly used
        /// within the views
        /// </summary>
        /// <param name="bundles">bundles is a BundleCollection that contains all of the registered bundles</param> 
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-3.1.1.min.js",
                        "~/Scripts/bootstrap.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-2.6.2.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryVal").Include(
                        "~/Scripts/jquery.validate.js",
                        "~/Scripts/jquery.validate.unobtrusive.js"));

            bundles.Add(new ScriptBundle("~/bundles/foolproofVal").Include(
                        "~/Scripts/jquery.validate.js",
                        "~/Scripts/jquery.validate.unobtrusive.js",
                        "~/Scripts/MvcFoolproofJQueryValidation.js",
                        "~/Scripts/mvcfoolproof.unobtrusive.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryAjax").Include(
                        "~/Scripts/jquery-3.1.1.min.js",
                        "~/Scripts/jquery.unobtrusive-ajax.min.js",
                        "~/Scripts/bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/fontAwesome/css").Include(
                        "~/Content/font-awesome.min.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/site.css"));
        }
    }
}
